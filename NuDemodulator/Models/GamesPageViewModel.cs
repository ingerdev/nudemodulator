﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.CoreParts;
using NuDemodulator.Datatypes;
using NuDemodulator.Managers;

namespace NuDemodulator.Models
{

    
    public class GamesPageViewModel:INotifyPropertyChanged
    {
        private ObservableCollection<UnitedGameDescription> _games;

        public ObservableCollection<UnitedGameDescription> Games
        {
            get { return _games; }
            set { _games = value; NotifyPropertyChanged("Games");}
        }

        public ICoreNavigationCommands NavigationMgr;
        public InfoCenter InfoCenter;
        public IGamePageNavigationParameters PageParameters;
        public GamesPageViewModel(InfoCenter infoCenter, ICoreNavigationCommands navigationMgr,
            IGamePageNavigationParameters pageParams)
        {
            InfoCenter = infoCenter;
            NavigationMgr = navigationMgr;
            Games= new ObservableCollection<UnitedGameDescription>();
            PageParameters = pageParams;
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }

}
