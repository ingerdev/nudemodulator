﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.CoreParts;
using NuDemodulator.Managers;

namespace NuDemodulator.Models
{
    public class GamePageViewModel : INotifyPropertyChanged
    {
        private UnitedGameDescription _game;
        public UnitedGameDescription Game
        {
            get { return _game; }
            set { _game = value; NotifyPropertyChanged("Game"); }
        }

        private int _raceId;
        public int RaceId
        {
            get { return _raceId; }
            set { _raceId = value;NotifyPropertyChanged("RaceId"); }
        }

        private bool _gameLoaded = false;
        public bool GameLoaded 
        {
            get { return _gameLoaded; }
            set { _gameLoaded = value; NotifyPropertyChanged("GameLoaded"); }
        }

        public ICoreNavigationCommands NavigationMgr;
        public InfoCenter InfoCenter;


        public IGamePageNavigationParameters PageParameters;
        


        public GamePageViewModel(InfoCenter infoCenter, ICoreNavigationCommands navigationMgr,
            IGamePageNavigationParameters pageParams)
        {
            InfoCenter = infoCenter;
            NavigationMgr = navigationMgr;
            PageParameters = pageParams;
        }



        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
