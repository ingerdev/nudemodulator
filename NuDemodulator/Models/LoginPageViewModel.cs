﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using NuDemodulator.CoreParts;
using NuDemodulator.Managers;
using NuDemodulator.Resources.LocalizedStrings;

namespace NuDemodulator.Models
{
    public class LoginPageViewModel:INotifyPropertyChanged,INotifyPropertyChanging,IDataErrorInfo
    {
        private Dictionary<String, bool> _fieldsState;
       
        public ICommand LoginCommand { get; set; }

        private string _login;
       

        public String Login
        {
            get { return _login; }
            set { _login = value; }
        }

        private string _password;
        public String Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string this[string columnName]
        {
            
            get
            {
                switch (columnName)
                {
                    case "Login": if (String.IsNullOrEmpty(Login))
                    {
                        _fieldsState[columnName] = false;
                        return AppResources.LoginScreen_Login;
                    }
                        break;
                    case "Password": if (String.IsNullOrEmpty(Login))
                        {
                            _fieldsState[columnName] = false;
                            return AppResources.LoginScreen_Password;
                        }
                        break;
                }
                return String.Empty;
            }
            
            
        }

         public ICoreNavigationCommands NavigationMgr;
        public InfoCenter InfoCenter;
        public LoginPageViewModel(InfoCenter infoCenter,ICoreNavigationCommands navigationMgr)
        {
            InfoCenter = infoCenter;
            NavigationMgr = navigationMgr;

            Login = "ingersol";
            Password = "";
        }

       

        public string Error { get; private set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion

    }
}
