﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuDemodulator.Datatypes
{

    public class GameInfo
    {
        public Game game { get; set; }
        public string yearfrom { get; set; }
        public string yearto { get; set; }
        public string wincondition { get; set; }
        public string schedule { get; set; }
        public string timetohost { get; set; }
        public bool haspassword { get; set; }
        public Settings settings { get; set; }
        public List<Player> players { get; set; }
        public List<Relation> relations { get; set; }
    }
}
