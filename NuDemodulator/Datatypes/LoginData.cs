﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace NuDemodulator.Parsers
{
    public class LoginSettings
    {
        public string myplanetfrom { get; set; }
        public string myplanetto { get; set; }
        public string enemyplanetfrom { get; set; }
        public string enemyplanetto { get; set; }
        public string allyplanetfrom { get; set; }
        public string allyplanetto { get; set; }
        public string infoplanetfrom { get; set; }
        public string infoplanetto { get; set; }
        public string unknownplanetfrom { get; set; }
        public string unknownplanetto { get; set; }
        public string myshipfrom { get; set; }
        public string myshipto { get; set; }
        public string enemyshipfrom { get; set; }
        public string enemyshipto { get; set; }
        public string allyshipfrom { get; set; }
        public string allyshipto { get; set; }
        public string mymines { get; set; }
        public string enemymines { get; set; }
        public string webmines { get; set; }
        public string allymines { get; set; }
        public string ionstorms { get; set; }
        public string soundon { get; set; }
        public string musicon { get; set; }
        public int battletutorialid { get; set; }
        public int battletaskid { get; set; }
        public bool assistanton { get; set; }
        public bool mousezoom { get; set; }
        public int id { get; set; }
    }

    public class LoginData
    {
        public bool success { get; set; }
        public string apikey { get; set; }
        public LoginSettings settings { get; set; }

        public static LoginData Parse(string loginJson)
        {
            return JsonConvert.DeserializeObject<LoginData>(loginJson);    
        }
    }
  
}