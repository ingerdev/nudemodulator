﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuDemodulator.Datatypes
{

    public class Settings
    {
        public string name { get; set; }
        public int turn { get; set; }
        public int buildqueueplanetid { get; set; }
        public int victorycountdown { get; set; }
        public int maxallies { get; set; }
        public int mapwidth { get; set; }
        public int mapheight { get; set; }
        public int numplanets { get; set; }
        public int shiplimit { get; set; }
        public string hoststart { get; set; }
        public string hostcompleted { get; set; }
        public string nexthost { get; set; }
        public string lastinvite { get; set; }
        public int teamsize { get; set; }
        public int planetscanrange { get; set; }
        public int shipscanrange { get; set; }
        public bool allvisible { get; set; }
        public bool minefieldsvisible { get; set; }
        public int nebulas { get; set; }
        public int stars { get; set; }
        public string discussionid { get; set; }
        public bool nuionstorms { get; set; }
        public int maxions { get; set; }
        public int maxioncloudsperstorm { get; set; }
        public int debrisdiskpercent { get; set; }
        public int debrisdiskversion { get; set; }
        public int cloakfail { get; set; }
        public int structuredecayrate { get; set; }
        public int mapshape { get; set; }
        public int verycloseplanets { get; set; }
        public int closeplanets { get; set; }
        public int otherplanetsminhomeworlddist { get; set; }
        public int ncircles { get; set; }
        public int hwdistribution { get; set; }
        public int ndebrisdiscs { get; set; }
        public int levelid { get; set; }
        public int nextlevelid { get; set; }
        public bool killrace { get; set; }
        public int runningstart { get; set; }
        public int deadradius { get; set; }
        public bool playerselectrace { get; set; }
        public int militaryscorepercent { get; set; }
        public bool hideraceselection { get; set; }
        public bool fixedstartpositions { get; set; }
        public int minnativeclans { get; set; }
        public int maxnativeclans { get; set; }
        public bool homeworldhasstarbase { get; set; }
        public int homeworldclans { get; set; }
        public int homeworldresources { get; set; }
        public string gamepassword { get; set; }
        public double neutroniumlevel { get; set; }
        public double duraniumlevel { get; set; }
        public double tritaniumlevel { get; set; }
        public double molybdenumlevel { get; set; }
        public int averagedensitypercent { get; set; }
        public int developmentfactor { get; set; }
        public int nativeprobability { get; set; }
        public int nativegovernmentlevel { get; set; }
        public int neusurfacemax { get; set; }
        public int dursurfacemax { get; set; }
        public int trisurfacemax { get; set; }
        public int molsurfacemax { get; set; }
        public int neugroundmax { get; set; }
        public int durgroundmax { get; set; }
        public int trigroundmax { get; set; }
        public int molgroundmax { get; set; }
        public bool computerbuildships { get; set; }
        public int computerbuilddelay { get; set; }
        public int fightorfail { get; set; }
        public bool showallexplosions { get; set; }
        public bool campaignmode { get; set; }
        public int maxadvantage { get; set; }
        public bool fascistdoublebeams { get; set; }
        public bool productionqueue { get; set; }
        public int productionbasecost { get; set; }
        public int productionstarbaseoutput { get; set; }
        public int productionstarbasereward { get; set; }
        public int endturn { get; set; }
        public int id { get; set; }
    }



   
    public class Game
    {
        public string name { get; set; }
        public string description { get; set; }
        public string shortdescription { get; set; }
        public int status { get; set; }
        public string datecreated { get; set; }
        public string dateended { get; set; }
        public int maptype { get; set; }
        public int gametype { get; set; }
        public int wincondition { get; set; }
        public double difficulty { get; set; }
        public int tutorialid { get; set; }
        public int requiredlevelid { get; set; }
        public int maxlevelid { get; set; }
        public int masterplanetid { get; set; }
        public int quadrant { get; set; }
        public int mintenacity { get; set; }
        public int faststart { get; set; }
        public int turnsperweek { get; set; }
        public int yearstarted { get; set; }
        public bool isprivate { get; set; }
        public int scenarioid { get; set; }
        public string createdby { get; set; }
        public int turn { get; set; }
        public int slots { get; set; }
        public string turnstatus { get; set; }
        public string hostdays { get; set; }
        public string slowhostdays { get; set; }
        public string hosttime { get; set; }
        public string lastbackuppath { get; set; }
        public string nexthost { get; set; }
        public bool allturnsin { get; set; }
        public bool lastnotified { get; set; }
        public bool ishosting { get; set; }
        public string lastloadeddate { get; set; }
        public string deletedate { get; set; }
        public string lasthostdate { get; set; }
        public string password { get; set; }
        public bool haspassword { get; set; }
        public string statusname { get; set; }
        public bool justended { get; set; }
        public string timetohost { get; set; }
        public int id { get; set; }
    }

    public class Player
    {
        public int status { get; set; }
        public int statusturn { get; set; }
        public int accountid { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public int raceid { get; set; }
        public int teamid { get; set; }
        public int prioritypoints { get; set; }
        public int joinrank { get; set; }
        public int finishrank { get; set; }
        public int turnjoined { get; set; }
        public bool turnready { get; set; }
        public string turnreadydate { get; set; }
        public int turnstatus { get; set; }
        public int turnsmissed { get; set; }
        public int turnsmissedtotal { get; set; }
        public int turnsholiday { get; set; }
        public int turnsearly { get; set; }
        public string activehulls { get; set; }
        public string activeadvantages { get; set; }
        public string savekey { get; set; }
        public int tutorialid { get; set; }
        public int tutorialtaskid { get; set; }
        public int id { get; set; }
    }

    public class AlienPlayer
    {
        public int status { get; set; }
        public int statusturn { get; set; }
        public int accountid { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public int raceid { get; set; }
        public int teamid { get; set; }
        public int prioritypoints { get; set; }
        public int joinrank { get; set; }
        public int finishrank { get; set; }
        public int turnjoined { get; set; }
        public bool turnready { get; set; }
        public string turnreadydate { get; set; }
        public int turnstatus { get; set; }
        public int turnsmissed { get; set; }
        public int turnsmissedtotal { get; set; }
        public int turnsholiday { get; set; }
        public int turnsearly { get; set; }
        public string activehulls { get; set; }
        public string activeadvantages { get; set; }
        public string savekey { get; set; }
        public int tutorialid { get; set; }
        public int tutorialtaskid { get; set; }
        public int id { get; set; }
    }

    public class Score
    {
        public string dateadded { get; set; }
        public int ownerid { get; set; }
        public int accountid { get; set; }
        public int capitalships { get; set; }
        public int freighters { get; set; }
        public int planets { get; set; }
        public int starbases { get; set; }
        public int militaryscore { get; set; }
        public int inventoryscore { get; set; }
        public int prioritypoints { get; set; }
        public int turn { get; set; }
        public double percent { get; set; }
        public int id { get; set; }
        public int shipchange { get; set; }
        public int freighterchange { get; set; }
        public int planetchange { get; set; }
        public int starbasechange { get; set; }
        public int militarychange { get; set; }
        public int inventorychange { get; set; }
        public int prioritypointchange { get; set; }
        public double percentchange { get; set; }
    }

    public class Planet
    {
        public string name { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public string friendlycode { get; set; }
        public int mines { get; set; }
        public int factories { get; set; }
        public int defense { get; set; }
        public int targetmines { get; set; }
        public int targetfactories { get; set; }
        public int targetdefense { get; set; }
        public int builtmines { get; set; }
        public int builtfactories { get; set; }
        public int builtdefense { get; set; }
        public bool buildingstarbase { get; set; }
        public int megacredits { get; set; }
        public int supplies { get; set; }
        public int suppliessold { get; set; }
        public int neutronium { get; set; }
        public int molybdenum { get; set; }
        public int duranium { get; set; }
        public int tritanium { get; set; }
        public int groundneutronium { get; set; }
        public int groundmolybdenum { get; set; }
        public int groundduranium { get; set; }
        public int groundtritanium { get; set; }
        public int densityneutronium { get; set; }
        public int densitymolybdenum { get; set; }
        public int densityduranium { get; set; }
        public int densitytritanium { get; set; }
        public int totalneutronium { get; set; }
        public int totalmolybdenum { get; set; }
        public int totalduranium { get; set; }
        public int totaltritanium { get; set; }
        public int checkneutronium { get; set; }
        public int checkmolybdenum { get; set; }
        public int checkduranium { get; set; }
        public int checktritanium { get; set; }
        public int checkmegacredits { get; set; }
        public int checksupplies { get; set; }
        public int temp { get; set; }
        public int ownerid { get; set; }
        public int clans { get; set; }
        public int colchange { get; set; }
        public int colonisttaxrate { get; set; }
        public int colonisthappypoints { get; set; }
        public int colhappychange { get; set; }
        public int nativeclans { get; set; }
        public int nativechange { get; set; }
        public int nativegovernment { get; set; }
        public int nativetaxvalue { get; set; }
        public int nativetype { get; set; }
        public int nativetaxrate { get; set; }
        public int nativehappypoints { get; set; }
        public int nativehappychange { get; set; }
        public int infoturn { get; set; }
        public int debrisdisk { get; set; }
        public int flag { get; set; }
        public int readystatus { get; set; }
        public string img { get; set; }
        public string nativeracename { get; set; }
        public string nativegovernmentname { get; set; }
        public int id { get; set; }
    }

    public class Ship
    {
        public string friendlycode { get; set; }
        public string name { get; set; }
        public int warp { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int beams { get; set; }
        public int bays { get; set; }
        public int torps { get; set; }
        public int mission { get; set; }
        public int mission1target { get; set; }
        public int mission2target { get; set; }
        public int enemy { get; set; }
        public int damage { get; set; }
        public int crew { get; set; }
        public int clans { get; set; }
        public int neutronium { get; set; }
        public int tritanium { get; set; }
        public int duranium { get; set; }
        public int molybdenum { get; set; }
        public int supplies { get; set; }
        public int ammo { get; set; }
        public int megacredits { get; set; }
        public int transferclans { get; set; }
        public int transferneutronium { get; set; }
        public int transferduranium { get; set; }
        public int transfertritanium { get; set; }
        public int transfermolybdenum { get; set; }
        public int transfersupplies { get; set; }
        public int transferammo { get; set; }
        public int transfermegacredits { get; set; }
        public int transfertargetid { get; set; }
        public int transfertargettype { get; set; }
        public int targetx { get; set; }
        public int targety { get; set; }
        public int mass { get; set; }
        public int heading { get; set; }
        public int turn { get; set; }
        public int turnkilled { get; set; }
        public int beamid { get; set; }
        public int engineid { get; set; }
        public int hullid { get; set; }
        public int ownerid { get; set; }
        public int torpedoid { get; set; }
        public int experience { get; set; }
        public int infoturn { get; set; }
        public int goal { get; set; }
        public int goaltarget { get; set; }
        public int goaltarget2 { get; set; }
        public List<object> waypoints { get; set; }
        public List<object> history { get; set; }
        public bool iscloaked { get; set; }
        public int readystatus { get; set; }
        public int id { get; set; }
    }

    public class Ionstorm
    {
        public int x { get; set; }
        public int y { get; set; }
        public int radius { get; set; }
        public int voltage { get; set; }
        public int warp { get; set; }
        public int heading { get; set; }
        public bool isgrowing { get; set; }
        public int parentid { get; set; }
        public int id { get; set; }
    }

    public class Starbas
    {
        public int defense { get; set; }
        public int builtdefense { get; set; }
        public int damage { get; set; }
        public int enginetechlevel { get; set; }
        public int hulltechlevel { get; set; }
        public int beamtechlevel { get; set; }
        public int torptechlevel { get; set; }
        public int hulltechup { get; set; }
        public int enginetechup { get; set; }
        public int beamtechup { get; set; }
        public int torptechup { get; set; }
        public int fighters { get; set; }
        public int builtfighters { get; set; }
        public int shipmission { get; set; }
        public int mission { get; set; }
        public int planetid { get; set; }
        public int raceid { get; set; }
        public int targetshipid { get; set; }
        public int buildbeamid { get; set; }
        public int buildengineid { get; set; }
        public int buildtorpedoid { get; set; }
        public int buildhullid { get; set; }
        public int buildbeamcount { get; set; }
        public int buildtorpcount { get; set; }
        public bool isbuilding { get; set; }
        public int starbasetype { get; set; }
        public int infoturn { get; set; }
        public int readystatus { get; set; }
        public int id { get; set; }
    }

    public class Stock
    {
        public int starbaseid { get; set; }
        public int stocktype { get; set; }
        public int stockid { get; set; }
        public int amount { get; set; }
        public int builtamount { get; set; }
        public int id { get; set; }
    }

    public class Relation
    {
        public int playerid { get; set; }
        public int playertoid { get; set; }
        public int relationto { get; set; }
        public int relationfrom { get; set; }
        public int conflictlevel { get; set; }
        public string color { get; set; }
        public int id { get; set; }
    }

    public class Message
    {
        //##############################################
        //this helper enum is added by NuDemodulator.
        public enum MessageType { Explosion = 10 }
        //##############################################

        public int ownerid { get; set; }
        public int messagetype { get; set; }
        public string headline { get; set; }
        public string body { get; set; }
        public int target { get; set; }
        public int turn { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int id { get; set; }
    }

    public class Mymessage
    {
        public int ownerid { get; set; }
        public int messagetype { get; set; }
        public string headline { get; set; }
        public string body { get; set; }
        public int target { get; set; }
        public int turn { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int id { get; set; }
    }

    public class Note
    {
        public int ownerid { get; set; }
        public string body { get; set; }
        public int targetid { get; set; }
        public int targettype { get; set; }
        public string color { get; set; }
        public int id { get; set; }
    }

    public class Left
    {
        public int vcrid { get; set; }
        public int objectid { get; set; }
        public string name { get; set; }
        public int side { get; set; }
        public int beamcount { get; set; }
        public int launchercount { get; set; }
        public int baycount { get; set; }
        public int hullid { get; set; }
        public int beamid { get; set; }
        public int torpedoid { get; set; }
        public int shield { get; set; }
        public int damage { get; set; }
        public int crew { get; set; }
        public int mass { get; set; }
        public int raceid { get; set; }
        public int beamkillbonus { get; set; }
        public int beamchargerate { get; set; }
        public int torpchargerate { get; set; }
        public int torpmisspercent { get; set; }
        public int crewdefensepercent { get; set; }
        public int torpedos { get; set; }
        public int fighters { get; set; }
        public int temperature { get; set; }
        public bool hasstarbase { get; set; }
        public int id { get; set; }
    }

    public class Right
    {
        public int vcrid { get; set; }
        public int objectid { get; set; }
        public string name { get; set; }
        public int side { get; set; }
        public int beamcount { get; set; }
        public int launchercount { get; set; }
        public int baycount { get; set; }
        public int hullid { get; set; }
        public int beamid { get; set; }
        public int torpedoid { get; set; }
        public int shield { get; set; }
        public int damage { get; set; }
        public int crew { get; set; }
        public int mass { get; set; }
        public int raceid { get; set; }
        public int beamkillbonus { get; set; }
        public int beamchargerate { get; set; }
        public int torpchargerate { get; set; }
        public int torpmisspercent { get; set; }
        public int crewdefensepercent { get; set; }
        public int torpedos { get; set; }
        public int fighters { get; set; }
        public int temperature { get; set; }
        public bool hasstarbase { get; set; }
        public int id { get; set; }
    }

    public class Vcr
    {
        public int seed { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int battletype { get; set; }
        public int leftownerid { get; set; }
        public int rightownerid { get; set; }
        public int turn { get; set; }
        public int id { get; set; }
        public Left left { get; set; }
        public Right right { get; set; }
    }

    public class Race
    {
        public string name { get; set; }
        public string shortname { get; set; }
        public string adjective { get; set; }
        public string baseadvantages { get; set; }
        public string advantages { get; set; }
        public string basehulls { get; set; }
        public string hulls { get; set; }
        public int id { get; set; }
    }

    public class Hull
    {
        public string name { get; set; }
        public int tritanium { get; set; }
        public int duranium { get; set; }
        public int molybdenum { get; set; }
        public int fueltank { get; set; }
        public int crew { get; set; }
        public int engines { get; set; }
        public int mass { get; set; }
        public int techlevel { get; set; }
        public int cargo { get; set; }
        public int fighterbays { get; set; }
        public int launchers { get; set; }
        public int beams { get; set; }
        public bool cancloak { get; set; }
        public int cost { get; set; }
        public string special { get; set; }
        public string description { get; set; }
        public int advantage { get; set; }
        public bool isbase { get; set; }
        public int dur { get; set; }
        public int tri { get; set; }
        public int mol { get; set; }
        public int mc { get; set; }
        public int parentid { get; set; }
        public int id { get; set; }
    }

    public class Beam
    {
        public string name { get; set; }
        public int cost { get; set; }
        public int tritanium { get; set; }
        public int duranium { get; set; }
        public int molybdenum { get; set; }
        public int mass { get; set; }
        public int techlevel { get; set; }
        public int crewkill { get; set; }
        public int damage { get; set; }
        public int id { get; set; }
    }

    public class Engine
    {
        public string name { get; set; }
        public int cost { get; set; }
        public int tritanium { get; set; }
        public int duranium { get; set; }
        public int molybdenum { get; set; }
        public int techlevel { get; set; }
        public int warp1 { get; set; }
        public int warp2 { get; set; }
        public int warp3 { get; set; }
        public int warp4 { get; set; }
        public int warp5 { get; set; }
        public int warp6 { get; set; }
        public int warp7 { get; set; }
        public int warp8 { get; set; }
        public int warp9 { get; set; }
        public int id { get; set; }
    }

    public class Torpedo
    {
        public string name { get; set; }
        public int torpedocost { get; set; }
        public int launchercost { get; set; }
        public int tritanium { get; set; }
        public int duranium { get; set; }
        public int molybdenum { get; set; }
        public int mass { get; set; }
        public int techlevel { get; set; }
        public int crewkill { get; set; }
        public int damage { get; set; }
        public int id { get; set; }
    }

    public class Minefield
    {
        public int ownerid { get; set; }
        public bool isweb { get; set; }
        public int units { get; set; }
        public int infoturn { get; set; }
        public string friendlycode { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int radius { get; set; }
        public int id { get; set; }
    }

    public class Advantage
    {
        public string name { get; set; }
        public string description { get; set; }
        public int value { get; set; }
        public bool isbase { get; set; }
        public bool locked { get; set; }
        public int dur { get; set; }
        public int tri { get; set; }
        public int mol { get; set; }
        public int mc { get; set; }
        public int id { get; set; }
    }

    public class Rst
    {
        public Settings settings { get; set; }
        public Game game { get; set; }
        public Player player { get; set; }
        public List<AlienPlayer> players { get; set; }
        public List<Score> scores { get; set; }
        public List<object> maps { get; set; }
        public List<Planet> planets { get; set; }
        public List<Ship> ships { get; set; }
        public List<Ionstorm> ionstorms { get; set; }
        public List<object> nebulas { get; set; }
        public List<object> stars { get; set; }
        public List<Starbas> starbases { get; set; }
        public List<Stock> stock { get; set; }
        public List<Minefield> minefields { get; set; }
        public List<Relation> relations { get; set; }
        public List<Message> messages { get; set; }
        public List<Mymessage> mymessages { get; set; }
        public List<Note> notes { get; set; }
        public List<Vcr> vcrs { get; set; }
        public List<Race> races { get; set; }
        public List<Hull> hulls { get; set; }
        public List<int> racehulls { get; set; }
        public List<Beam> beams { get; set; }
        public List<Engine> engines { get; set; }
        public List<Torpedo> torpedos { get; set; }
        public List<Advantage> advantages { get; set; }
    }

    public class Accountsettings
    {
        public string myplanetfrom { get; set; }
        public string myplanetto { get; set; }
        public string enemyplanetfrom { get; set; }
        public string enemyplanetto { get; set; }
        public string allyplanetfrom { get; set; }
        public string allyplanetto { get; set; }
        public string infoplanetfrom { get; set; }
        public string infoplanetto { get; set; }
        public string unknownplanetfrom { get; set; }
        public string unknownplanetto { get; set; }
        public string myshipfrom { get; set; }
        public string myshipto { get; set; }
        public string enemyshipfrom { get; set; }
        public string enemyshipto { get; set; }
        public string allyshipfrom { get; set; }
        public string allyshipto { get; set; }
        public string mymines { get; set; }
        public string enemymines { get; set; }
        public string webmines { get; set; }
        public string allymines { get; set; }
        public string ionstorms { get; set; }
        public string soundon { get; set; }
        public string musicon { get; set; }
        public int battletutorialid { get; set; }
        public int battletaskid { get; set; }
        public bool assistanton { get; set; }
        public bool mousezoom { get; set; }
        public int id { get; set; }
    }

    public class GameData
    {
        public bool success { get; set; }
        public Rst rst { get; set; }
        public Accountsettings accountsettings { get; set; }
        public string savekey { get; set; }
        public bool ispremium { get; set; }
    }
   
}
