﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace NuDemodulator.Managers
{
    public interface ICoreNavigationCommands
    {
        void navigateLoginPage();
        void navigateGamesPage();
        void navigateGamePage(int gameId);
        void navigateTurnPage();
    }
    public class NavigationCommands: ICoreNavigationCommands
    {
        private const String URI_LOGIN_PAGE = "/NuDemodulator;component/gui/pages/LoginPage.xaml";
        private const String URI_GAMES_PAGE = "/NuDemodulator;component/gui/pages/GamesPage.xaml";
        private const String URI_TURN_PAGE = "/NuDemodulator;component/gui/pages/TurnPage.xaml";
        private const String URI_GAME_PAGE = "/NuDemodulator;component/gui/pages/GamePage.xaml";

        public void navigateLoginPage()
        {           
            getFrame().Navigate(new Uri(URI_LOGIN_PAGE, UriKind.Relative));
        }

        public void navigateGamesPage()
        {

            getFrame().Navigate(new Uri(URI_GAMES_PAGE, UriKind.Relative));
        }

        public void navigateGamePage(int gameId)
        {
          
            getFrame().Navigate(new Uri(URI_GAME_PAGE, UriKind.Relative), gameId);
        }

        public void navigateTurnPage()
        {
            getFrame().Navigate(new Uri(URI_TURN_PAGE, UriKind.Relative));
        }

        public static Frame getFrame()
        {
            return (Application.Current.MainWindow.FindName("NavigationFrame") as Frame);
        }
    }

   
}
