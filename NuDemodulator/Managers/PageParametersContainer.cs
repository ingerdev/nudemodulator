﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;

namespace NuDemodulator.Managers
{
    public interface IGamePageNavigationParameters
    {
        int CurrentGameId { get; set; }
    }

    //this class contain parameters that should be transferred between pages
    //unfortunatelly, every native WPF way to do this suck.
    public class PageParametersContainer : IGamePageNavigationParameters
    {
        #region IGamePageNavigationParameters

        //anyone can add here any properties.
        public int CurrentGameId { get; set; }

        #endregion
    }
}