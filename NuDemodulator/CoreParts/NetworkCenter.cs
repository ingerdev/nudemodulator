﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;

namespace NuDemodulator.CoreParts
{
    public class NetworkCenter //:INetworkLogin
    {
        public static async Task<string> login(string username, string password)
        {
            using (var client = new HttpClient(new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip
                                         | DecompressionMethods.Deflate
            }))
            {
                var values = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("username", username),
                    new KeyValuePair<string, string>("password", password)
                };

                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync("http://api.planets.nu/login", content);

                return await response.Content.ReadAsStringAsync();
            }
        }

        public static async Task<string> loadGamesList(string username)
        {
            using (var client = new HttpClient(new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip
                                         | DecompressionMethods.Deflate
            }))
            {
                var response =
                    await
                        client.GetAsync(String.Format("http://api.planets.nu/games/list?username={0}",
                            Uri.EscapeDataString(username)));
                return await response.Content.ReadAsStringAsync();
            }
        }
        public static async Task<string> loadGamePublicInfo(int gameId)
        {
            using (var client = new HttpClient(new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip
                                         | DecompressionMethods.Deflate
            }))
            {
                var response =
                    await 
                        client.GetAsync(String.Format("http://api.planets.nu/game/loadinfo?gameid={0}",
                            Uri.EscapeDataString(gameId.ToString(CultureInfo.InvariantCulture))));
                return await response.Content.ReadAsStringAsync();
            }
        }

        public static async Task<string> loadGameTurn(string apikey, int gameId)
        {
            using (var client = new HttpClient(new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip
                                         | DecompressionMethods.Deflate
            }))
            {
                var response = await client.GetAsync(String.Format("http://api.planets.nu/game/loadturn?gameid={1}&apikey={0}", Uri.EscapeDataString(apikey),
                            Uri.EscapeDataString(gameId.ToString(CultureInfo.InvariantCulture))));
                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}