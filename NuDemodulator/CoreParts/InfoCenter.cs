﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NuDemodulator.Datatypes;
using NuDemodulator.Models;
using NuDemodulator.Parsers;

namespace NuDemodulator.CoreParts
{
    //contain all Game info + some fields were copied as dedicated properties
    //for better visual xaml binding purposes
    public class UnitedGameDescription
    {
        public GameInfo GameInfo { get; set; }
        public GameData GameData { get; set; }
        public Player Player { get; set; }
    }

    public class InfoCenter
    {
        public string UserLogin;
        public LoginData LoginData;
    
        //int = game id
        public Dictionary<int, UnitedGameDescription> Games = new Dictionary<int, UnitedGameDescription>();

        public static List<Game> ParseGames(string jsonedGames)
        {
            return JsonConvert.DeserializeObject<List<Game>>(jsonedGames); 
        }
        public static GameInfo ParseGameInfo(string jsonedData)
        {
            return JsonConvert.DeserializeObject<GameInfo>(jsonedData);
        }
        public static GameData ParseGameData(string jsonedData)
        {
            return JsonConvert.DeserializeObject<GameData>(jsonedData);
        }
    }
}
