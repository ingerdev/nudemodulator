﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace NuDemodulator.CoreParts
{
    public interface INetworkLogin
    {
         Task<string> login(string username, string password);

    }
}
