﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace NuDemodulator.WPFConverters
{
    public class BoolToValueConverter<T> : IValueConverter
    {
        public T FalseValue { get; set; }
        public T TrueValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return FalseValue;
            else
                return (bool)value ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null ? value.Equals(TrueValue) : false;
        }
    }

    public class BoolToVisibilityConverter : BoolToValueConverter<System.Windows.Visibility> { }
    public class BoolToDoubleConverter : BoolToValueConverter<Double> { }
    public class BoolToBoolConverter : BoolToValueConverter<Boolean> { }
    public class BoolToBrushConverter : BoolToValueConverter<SolidColorBrush> { }
    public class BoolToOpacityConverter : BoolToValueConverter<Double> { }
    public class BoolToStringConverter : BoolToValueConverter<String> { }
    public class BoolToColorConverter : BoolToValueConverter<Color> { }

    public class BoolToImageSourceConverter : BoolToValueConverter<ImageSource> { }

}
