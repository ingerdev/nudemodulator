﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace NuDemodulator.WPFConverters
{
    public class SwitchConverter<T> : DependencyObject, IValueConverter
    {
        public static readonly DependencyProperty Value1Property =
            DependencyProperty.Register("Value1", typeof (T), typeof (SwitchConverter<T>),
                new PropertyMetadata(null));

        public T Value1
        {
            get { return (T)GetValue(Value1Property); }
            set { SetValue(Value1Property, value); }
        }

        public static readonly DependencyProperty Value2Property =
           DependencyProperty.Register("Value2", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value2
        {
            get { return (T)GetValue(Value2Property); }
            set { SetValue(Value2Property, value); }
        }

        public static readonly DependencyProperty Value3Property =
           DependencyProperty.Register("Value3", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value3
        {
            get { return (T)GetValue(Value3Property); }
            set { SetValue(Value3Property, value); }
        }

        public static readonly DependencyProperty Value4Property =
           DependencyProperty.Register("Value4", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value4
        {
            get { return (T)GetValue(Value4Property); }
            set { SetValue(Value4Property, value); }
        }

        public static readonly DependencyProperty Value5Property =
           DependencyProperty.Register("Value5", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value5
        {
            get { return (T)GetValue(Value5Property); }
            set { SetValue(Value5Property, value); }
        }

        public static readonly DependencyProperty Value6Property =
           DependencyProperty.Register("Value6", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value6
        {
            get { return (T)GetValue(Value6Property); }
            set { SetValue(Value6Property, value); }
        }

        public static readonly DependencyProperty Value7Property =
           DependencyProperty.Register("Value7", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value7
        {
            get { return (T)GetValue(Value7Property); }
            set { SetValue(Value7Property, value); }
        }

        public static readonly DependencyProperty Value8Property =
           DependencyProperty.Register("Value8", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value8
        {
            get { return (T)GetValue(Value8Property); }
            set { SetValue(Value8Property, value); }
        }

        public static readonly DependencyProperty Value9Property =
           DependencyProperty.Register("Value9", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value9
        {
            get { return (T)GetValue(Value9Property); }
            set { SetValue(Value9Property, value); }
        }

        public static readonly DependencyProperty Value10Property =
           DependencyProperty.Register("Value10", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value10
        {
            get { return (T)GetValue(Value10Property); }
            set { SetValue(Value10Property, value); }
        }

        public static readonly DependencyProperty Value11Property =
           DependencyProperty.Register("Value11", typeof(T), typeof(SwitchConverter<T>),
               new PropertyMetadata(null));

        public T Value11
        {
            get { return (T)GetValue(Value11Property); }
            set { SetValue(Value11Property, value); }
        }

       

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value == null)
                return DependencyProperty.UnsetValue;
            else
                switch ((int) value)
                {
                    case 1:
                        return Value1;
                    case 2:
                        return Value2;
                    case 3:
                        return Value3;
                    case 4:
                        return Value4;
                    case 5:
                        return Value5;
                    case 6:
                        return Value6;
                    case 7:
                        return Value7;
                    case 8:
                        return Value8;
                    case 9:
                        return Value9;
                    case 10:
                        return Value10;
                    case 11:
                        return Value11;
                   
                    default:
                        return DependencyProperty.UnsetValue;
                }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SwitchToImageSourceConverter:SwitchConverter<ImageSource>{}



}
