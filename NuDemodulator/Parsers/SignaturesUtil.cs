﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuDemodulator.Generators
{
    public static  class SignaturesUtil
    {
        public static byte[] encodePassword(string password)
        {
            //password should be 10-chars length
            if (password.Length>10)
                throw new ArgumentException("Password length shouldnt exceed 10 chars");

            byte[] bytePass = Encoding.ASCII.GetBytes(password.PadRight(10, '\0'));            
            byte[] storage= new byte[20];


            //+108 20 BYTEs   Password
            //    The password has 10 characters (padded with NULs). Each
            //    character of the password is decoded as follows:
            //      VAR pw : ARRAY[0..19] OF BYTE;    { this field }
            //          ch : ARRAY[0..9] OF CHAR;     { password }
            //      FOR i := 0 TO 9 DO
            //        ch[i] := Chr(pw[i] - pw[19-i] + 32);

            //password is dumb thing so i will not seed random in storage and let it be zeroed
            for(int i = 0; i<10;i++)
            {

                storage[i] = (byte)(bytePass[i] + storage[19 - i] - 32);
            }

            return storage;
        }

        public static byte[] GetSignature1FromPassword(string password)
        {

            var encodedPassword = encodePassword(password);
            byte[] signature1 = new byte[10];

            //            From this data, a file signature is constructed:
            //+118 10 BYTEs   (second part of password data)
            //                Signature 1 is these 10 bytes.
            Array.Copy(encodedPassword, 10, signature1, 0, 10);
            return signature1;


        }

        public static byte[] GetSignature2FromPassword(string password)
        {
            var encodedPassword = encodePassword(password);
            byte[] signature2 = new byte[10];

//            From this data, a file signature is constructed:
//+118 10 BYTEs   (second part of password data)
//                Signature 1 is these 10 bytes.
//                Increase the first byte by 1, the second by 2 and so on.
//                The result is Signature 2.
            for(byte i = 0;i<10;i++)
            {
                //todo: fix possible overflow
                signature2[i] =(byte)(encodedPassword[10 + i] + i);
            }

            return signature2;
        }

      
    }
}
