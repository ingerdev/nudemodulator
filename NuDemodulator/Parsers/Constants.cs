﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.TextFormatting;

namespace NuDemodulator.Parsers
{
    public class Constants
    {
        //visual scan information for ships, maximum count
        public const int MAX_TARGETS_COUNT = 50;

        //maximum length of planet file name
        public const int PLANET_NAME_FIXED_LENGTH_BYTES = 20;

        //maximum length of ship file name
        public const int SHIP_NAME_FIXED_LENGTH_BYTES = 20;

        //max length of ship hull name
        public const int HULL_NAME_FIXED_LENGTH_BYTES = 30;

        //max length of ship engine name
        public const int ENGINE_NAME_FIXED_LENGTH_BYTES = 20;

        //max length of ship beam name
        public const int BEAM_NAME_FIXED_LENGTH_BYTES = 20;

        //max length of ship torpedo name
        public const int TORP_NAME_FIXED_LENGTH_BYTES = 20;

        //max ship list length
        public const int SHIP_LIST_LENGTH_MAX = 20;

        public const String DEFAULT_PASSWORD = "NOPASSWORD";
    }
}
