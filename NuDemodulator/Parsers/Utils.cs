﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuDemodulator.Parsers
{
    public class Utils
    {
        // - trunc name to 20 symbols if need
        // - pads it with spaces
        // - convert to 20-bytes byte array regardless string encoding
        public static byte[] GetNormalizedShipName(string shipName)
        {
            return GetNormalizedString(shipName, Constants.SHIP_NAME_FIXED_LENGTH_BYTES);
        }
        public static byte[] GetNormalizedPlanetName(string planetName)
        {
            return GetNormalizedString(planetName, Constants.PLANET_NAME_FIXED_LENGTH_BYTES);
        }

        public static byte[] GetNormalizedHullName(string hullName)
        {
            return GetNormalizedString(hullName, Constants.HULL_NAME_FIXED_LENGTH_BYTES);
        }

        public static byte[] GetNormalizedEngineName(string engineName)
        {
            return GetNormalizedString(engineName, Constants.ENGINE_NAME_FIXED_LENGTH_BYTES);
        }
        public static byte[] GetNormalizedBeamName(string engineName)
        {
            return GetNormalizedString(engineName, Constants.BEAM_NAME_FIXED_LENGTH_BYTES);
        }

        public static byte[] GetNormalizedTorpedoName(string torpName)
        {
            return GetNormalizedString(torpName, Constants.TORP_NAME_FIXED_LENGTH_BYTES);
        }

        public static byte[] GetNormalizedString(string str, byte fixedStrLength)
        {
            return Encoding.ASCII.GetBytes(str.PadRight(fixedStrLength).Substring(0, fixedStrLength));
        }

        public static byte[] GetASCIIString(string str)
        {
            return Encoding.ASCII.GetBytes(str);
        }

        public static byte[] GetTimestamp(DateTime dateTime)
        {
            var stringDateTime = dateTime.ToString("mm-dd-yyyyhh:mm:ss");
            return Encoding.ASCII.GetBytes(stringDateTime);
        }


        public static byte[] GetTimeStampFromPlanetsNuTimestamp(string dateTime)
        {
            DateTime parsedDatetime;
            DateTime.TryParseExact(dateTime, @"M/d/yyyy h:mm:ss tt",
            //DateTime.TryParseExact(@"9/20/2014 11:02:07 AM", @"M/d/yyyy h:mm:ss tt",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDatetime);
            
            if (parsedDatetime.Year <2000)
                throw new ArgumentException(String.Format("Cannot parse turn timestamp {0}",dateTime));

            return GetTimestamp(parsedDatetime);
     
        }

        public static int GetTimestampChecksum(string dateTime)
        {
            var timestamp = GetTimeStampFromPlanetsNuTimestamp(dateTime);

//          +155    WORD    Checksum of the time stamp = sum of bytes at 0 to 17
            int sum = timestamp.Take(18).Sum(x => x);
            return sum;
        }
        
    }
}
