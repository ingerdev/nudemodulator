﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NuDemodulator.CoreParts;
using NuDemodulator.Models;
using NuDemodulator.Parsers;

namespace NuDemodulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainPageViewModel _vm;
        public MainWindow()
        {
            InitializeComponent();
            _vm = NavigationFrame.DataContext as MainPageViewModel;
            _vm.NavigationMgr.navigateLoginPage();
        }

          

      
    }
}
