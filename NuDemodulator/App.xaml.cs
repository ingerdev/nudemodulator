﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Ninject.Modules;
using NuDemodulator.Service;

namespace NuDemodulator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private AppBase _appBase;
        public App()
        {
            _appBase = AppBase.getInstance(this);
            _appBase.initNinjectModules(new NinjectModule[] { });

            _appBase.init();

        }
    }
}
