﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using NuDemodulator.Models;

namespace NuDemodulator.Service
{
    public class ViewModelLocator
    {
        //Common models

        public LoginPageViewModel LoginPageViewModel
        {
            get { return AppBase.getInstance(null).getIoCKernel().Get<LoginPageViewModel>(); }
        }
        public GamesPageViewModel GamesPageViewModel
        {
            get { return AppBase.getInstance(null).getIoCKernel().Get<GamesPageViewModel>(); }
        }

        public GamePageViewModel GamePageViewModel
        {
            get { return AppBase.getInstance(null).getIoCKernel().Get<GamePageViewModel>(); }
        }

        public MainPageViewModel MainPageViewModel
        {
            get { return AppBase.getInstance(null).getIoCKernel().Get<MainPageViewModel>(); }
        }
    }
}
