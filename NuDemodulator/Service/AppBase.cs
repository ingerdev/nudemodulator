﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ninject;
using Ninject.Modules;
using NuDemodulator.CoreParts;
using NuDemodulator.Ninject;

namespace NuDemodulator.Service
{
    public class AppBase
    {
        //singletoning
        private static AppBase _appBase;

     
        public static AppBase getInstance(Application app)
        {
            return _appBase ?? (_appBase = new AppBase(app));
        }

        //core of IoC subsystem
        private IKernel _iocKernel;

       
        private AppBase(Application app)
        {
          
        }

     

       


        //creating Ninject kernel service contained all dependency list
        //not: after creating kernel, no dependencies can be added
        public void initNinjectModules(IEnumerable<INinjectModule> additionalModules)
        {
            INinjectModule[] modules =
                additionalModules.Concat(new INinjectModule[]
                {
                    new NuDemodulatorNinjectModule(), 
                }).ToArray();
            _iocKernel = new StandardKernel(modules);
        }

        //!!note: typeName requires binding to object (see ninject module in common_free assembly for example)
        //and can be binded one type only
        public Object getDependencyAsObject(string typeName)
        {
            return _iocKernel.Get<object>(typeName);
        }


        public IKernel getIoCKernel()
        {
            return _iocKernel;
        }

      



        /// <summary>
        /// init classes which required present AppBase object
        /// </summary>
        public void init()
        {
            // initing language
            //_iocKernel.Get<ILanguageManager>();
            //init network center 
            _iocKernel.Get<NetworkCenter>();          
        }



    
    }
}
