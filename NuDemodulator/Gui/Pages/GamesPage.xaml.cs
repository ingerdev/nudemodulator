﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NuDemodulator.CoreParts;
using NuDemodulator.Datatypes;
using NuDemodulator.Models;

namespace NuDemodulator.Gui.Pages
{
    /// <summary>
    /// Interaction logic for GamesPage.xaml
    /// </summary>
    public partial class GamesPage : Page
    {
        private GamesPageViewModel _vm;

        public GamesPage()
        {
            InitializeComponent();
            _vm = (GamesScreen.DataContext as GamesPageViewModel);
            loadGames();
        }

        private void loadGames()
        {
            Observable.FromAsync(ct => NetworkCenter.loadGamesList(_vm.InfoCenter.UserLogin))
                .ObserveOnDispatcher()
                .Subscribe(answer =>
                {
                    var userGames = InfoCenter.ParseGames(answer);
                    foreach (var game in userGames)
                    {
                       loadGamePublicInfo(game);                        
                    }
                });

            // vm.InfoCenter.Games = NetworkCenter.loadGamesList();
        }

        private void Play_OnClick(object sender, RoutedEventArgs e)
        {
            UnitedGameDescription game = (UnitedGameDescription)((Button)sender).DataContext;
            _vm.PageParameters.CurrentGameId = game.GameInfo.game.id;
            _vm.NavigationMgr.navigateGamePage(game.GameInfo.game.id);
           
        }
        private void loadGamePublicInfo(Game game)
        {
            Observable.FromAsync(ct => NetworkCenter.loadGamePublicInfo(game.id))
                .ObserveOnDispatcher()
                .Subscribe(answer =>
                {
                    var gameInfo = InfoCenter.ParseGameInfo(answer);
                    var parsedGame = new UnitedGameDescription()
                    {
                        GameInfo = gameInfo,
                        Player =
                            gameInfo.players.FirstOrDefault(
                                player =>
                                    player.username.Equals(_vm.InfoCenter.UserLogin,
                                        StringComparison.InvariantCultureIgnoreCase)),
                    };
                    _vm.InfoCenter.Games[game.id] = parsedGame;
                    
                    //note, GameData field is empty
                    _vm.Games.Add(parsedGame);
                });
        }

        

  


     
    }
}