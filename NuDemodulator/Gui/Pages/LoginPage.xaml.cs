﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NuDemodulator.CoreParts;
using NuDemodulator.Models;
using NuDemodulator.Parsers;

namespace NuDemodulator.Gui.Pages
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        private LoginPageViewModel _vm;
        public LoginPage()
        {
            InitializeComponent();
            _vm = (LoginScreen.DataContext as LoginPageViewModel);
            PasswordBox.Password = _vm.Password;
            
        }

        private void LoginButton_OnClick(object sender, RoutedEventArgs e)
        {
            callLogin();

        }

        private void callLogin()
        {
            LoginButton.IsEnabled = false;
            var vm = (LoginScreen.DataContext as LoginPageViewModel);

            Observable.FromAsync(ct => NetworkCenter.login(vm.Login, PasswordBox.Password)).ObserveOnDispatcher().Subscribe(answer =>
            {
                LoginButton.IsEnabled = true;

                var login = LoginData.Parse(answer);
                if (login.success)
                {
                    vm.InfoCenter.UserLogin = vm.Login;
                    vm.InfoCenter.LoginData = login;                   
                }
                _vm.NavigationMgr.navigateGamesPage();
            });
        }
    }

}
