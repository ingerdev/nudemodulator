﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NuDemodulator.CoreParts;
using NuDemodulator.Generators;
using NuDemodulator.Models;
using NuDemodulator.Parsers;

namespace NuDemodulator.Gui.Pages
{
    /// <summary>
    /// Interaction logic for GamePage.xaml
    /// </summary>
    public partial class GamePage : Page
    {
        private GamePageViewModel _vm;

        public GamePage()
        {
            InitializeComponent();
            _vm = (GameScreen.DataContext as GamePageViewModel);

            _vm.Game = _vm.InfoCenter.Games[_vm.PageParameters.CurrentGameId];
            
            loadGameData(_vm.PageParameters.CurrentGameId);

        }


        private void loadGameData(int gameId)
        {
            Observable.FromAsync(ct => NetworkCenter.loadGameTurn(_vm.InfoCenter.LoginData.apikey, gameId))
                .ObserveOnDispatcher()
                .Subscribe(answer =>
                {
                    _vm.InfoCenter.Games[gameId].GameData = InfoCenter.ParseGameData(answer);
                    _vm.RaceId = _vm.Game.GameData.rst.player.raceid;
                    _vm.GameLoaded = true;
                });
        }


        /* Does not work. Switching to PageParameters singleton
        private void GamePage_OnLoaded(object sender, RoutedEventArgs e)
        {
            //NavigationService ns = NavigationService;
            NavigationService ns = NuDemodulator.Managers.NavigationCommands.getFrame().NavigationService;
            if (NavigationService != null)
            {
                NavigationService.LoadCompleted += NavigationService_LoadCompleted;
            }
        }

        private void NavigationService_LoadCompleted(object sender, NavigationEventArgs e)
        {
            NavigationService.LoadCompleted -= NavigationService_LoadCompleted;

            if (e.ExtraData == null)
            {
                throw new ArgumentNullException("TurnPage was nabigated without game id in parameters");
            }

            if (!(e.ExtraData is int))
            {
                throw new ArgumentException("TurnPage parameter is not of type int");
            }

            _vm.Game = _vm.InfoCenter.Games[(int) e.ExtraData];
            _vm.RaceId = _vm.Game.GameData.rst.player.raceid;
        }
         * */

        private void GetRST_OnClick(object sender, RoutedEventArgs e)
        {
            const string GAME_DIRECTORY = @"c:\temp\VGAPlanets\Tests\";
            var gameData = _vm.InfoCenter.Games[_vm.PageParameters.CurrentGameId].GameData;

            var stocks = gameData.rst.stock.Where(stock => stock.builtamount!=0).ToList();
            
            //Utils.GetTimeStampFromPlanetsNuTimestamp(gameData.rst.game.lasthostdate);

            //generate Planet.nm
            PlanetNM.GeneratePlanetsNMFile(gameData.rst.planets, String.Format("{0}{1}",GAME_DIRECTORY,@"planet.nm"));
            //generate xyplan.dat
            PlanetNM.GenerateXYPlanFile(gameData.rst.planets,String.Format("{0}{1}",GAME_DIRECTORY,@"xyplan.dat"));
            //generate race.nm
            RaceNM.GenerateRaceNMFile(gameData.rst.races, String.Format("{0}{1}",GAME_DIRECTORY,@"race.nm"));
            Hullspec.GenerateHullspecFile(gameData.rst.hulls, gameData.rst.player.raceid,
                String.Format("{0}{1}", GAME_DIRECTORY, @"hullspec.dat"));
            /*
            //generate shipx.dat
            ShipX.GenerateShipXFile(_vm.InfoCenter.Games[_vm.PageParameters.CurrentGameId].GameData.rst.ships, _vm.Game.GameData.rst.player.raceid, String.Format(@"c:\temp\VGAPlanets\Tests\ship{0}.dat", _vm.Game.GameData.rst.player.raceid));

            //generate targetx.dat , contain all enemy ships datas
            TargetX.GenerateTargetXFile(_vm.InfoCenter.Games[_vm.PageParameters.CurrentGameId].GameData.rst.ships, _vm.Game.GameData.rst.player.raceid, String.Format(@"c:\temp\VGAPlanets\Tests\target{0}.dat", _vm.Game.GameData.rst.player.raceid));
            */
            RstFileComposer composer = new RstFileComposer(gameData.rst,
                String.Format(@"{0}player{1}.rst", GAME_DIRECTORY,gameData.rst.player.raceid));


        }
        
    }
}