﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Modules;
using NuDemodulator.CoreParts;
using NuDemodulator.Managers;

namespace NuDemodulator.Ninject
{
    public class NuDemodulatorNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<PageParametersContainer>().ToSelf().InSingletonScope();
            Bind<IGamePageNavigationParameters>().ToMethod(c => c.Kernel.Get<PageParametersContainer>());    
            Bind<InfoCenter>().ToSelf().InSingletonScope();
            Bind<ICoreNavigationCommands>().To<NavigationCommands>().InSingletonScope();
        }
    }
}
