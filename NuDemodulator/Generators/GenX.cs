﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    public class GenX
    {
        public static void GenerateShipXYXFile(List<Score> scores,Game game, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    //ship array contain all ship list, our and alien
                    //so we should filter them

                    GenerateGenXSection(scores,game, raceid, fileStream);

                    // +m  10 BYTEs   Signature 2
                    writer.Write(SignaturesUtil.encodePassword(Constants.DEFAULT_PASSWORD));
                }

            }
        }

        //write values as they should be in proper rst section
        //purpose of serializeFull is: when it is on, then 
        //additional fields should be serialized to fulfill getx.dat file format
        //serializeFull = false is necessary when genx memory section requested
        //(for RST  file composing)
        public static void GenerateGenXSection(List<Score> scores,Game game, int raceid, Stream writeStream,bool serialializeFull = false)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
               
//                 +0  18 BYTEs   Timestamp (10 bytes date mm-dd-yyyy, 8 bytes time hh:mm:ss)
                writer.Write(Utils.GetTimeStampFromPlanetsNuTimestamp(game.lasthostdate));
//+18  88 BYTEs   11 score records of 8 bytes each:
                 
                foreach (var score in scores)
                {
//                 +0     WORD    Number of planets, 10 points each
                    writer.Write((ushort)score.planets);
//                 +2     WORD    Number of capital ships, 10 points each
                    writer.Write((ushort)score.capitalships);
//                 +4     WORD    Number of freighters, 1 point each
                    writer.Write((ushort)score.freighters);
//                 +6     WORD    Number of starbases, 120 points each
                    writer.Write((ushort)score.starbases);                                                             
                }
//+106    WORD    Player Id
                writer.Write((ushort) raceid);
//+108 20 BYTEs   Password
                writer.Write(SignaturesUtil.encodePassword(Constants.DEFAULT_PASSWORD));
//                The password has 10 characters (padded with NULs). Each
//                character of the password is decoded as follows:
//                  VAR pw : ARRAY[0..19] OF BYTE;    { this field }
//                      ch : ARRAY[0..9] OF CHAR;     { password }
//                  FOR i := 0 TO 9 DO
//                    ch[i] := Chr(pw[i] - pw[19-i] + 32);
//+128    BYTE    unused
                if (serialializeFull)
                {
//                This field may belong to the password data. If you define
//                an array of size 20 in BASIC, this field really gets 21
//                elements (0 to 20). The password only uses 0 to 19.
                    writer.Write((byte) 0);
                }
//+129    DWORD   Checksum of SHIPx.DAT/DIS
//+133    DWORD   Checksum of PDATAx.DAT/DIS
//+137    DWORD   Checksum of BDATAx.DAT/DIS
//                These checksums are the total sum of all bytes of the
//                appropriate files (i.e., count word, data, and signature
//                block of both files).
                if (serialializeFull)
                {
//+141    WORD    13 if the password has changed, 0 else
                    writer.Write((ushort)13);              
//+143 10 BYTEs   New password, if any: each byte is increased by 50
                    writer.Write((ulong) 0);
                    writer.Write((ushort) 0);
//+153    WORD    Turn number
                }
                writer.Write((ushort)game.turn);
//+155    WORD    Checksum of the time stamp = sum of bytes at 0 to 17
                writer.Write((ushort) Utils.GetTimestampChecksum(game.lasthostdate));
//From this data, a file signature is constructed:
//+118 10 BYTEs   (second part of password data)
//                Signature 1 is these 10 bytes.
//                Increase the first byte by 1, the second by 2 and so on.
//                The result is Signature 2.
            }
        }
    }
    
}
