﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    public class VcrX
    {
        public static void GenerateVcrXFile(List<Vcr> vcrs, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    //ship array contain all ship list, our and alien
                    //so we should filter them

                    GenerateVcrXSection(vcrs, raceid, fileStream);

                    // +m  10 BYTEs   Signature 2
                    writer.Write(SignaturesUtil.encodePassword(Constants.DEFAULT_PASSWORD));
                }

            }
        }

        //write values as they should be in proper rst section
        public static void GenerateVcrXSection(List<Vcr> vcrs, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
// +0     WORD    Number of VCRs. For PHost, see below.
                writer.Write((ushort) vcrs.Count);
              
                foreach (var vcr in vcrs)
                {
//                    A VCR record:
// +0     WORD    `init' Random Number Generator initial value.
//                Host:    Must be in range 1..119. VCR's random number
//                         generator uses a table of 119 "random numbers"
//                         used in sequence and scaled to the needed range.
//                         Host actually generates 1..111 (111 is very rare).
//                PHost:   any value is possible.
                    writer.Write((ushort) vcr.seed);
// +2     WORD    Signature (first battle only):
//                Host:    must be zero
//                PHost:   must be `48879-init' (modulo 65536). Zero on
//                         subsequent battles.
                    
//                c2nu:    must be 21838 ("NU" in ASCII).
                    writer.Write((ushort) 21838);
// +4     WORD    Host:    Temperature code of planet for a Ship/Planet battle
//                PHost:   Capability flags (first battle only):
//                          Bit 0    "Death rays" (new weapon possibilities of
//                                   PHost 4.0: weapons with Expl=0 in use, or
//                                   ShieldKillScaling!=0)
//                          Bit 1    "Ship experience levels" (experience
//                                   system of PHost 4.0 is in use)
//                          Bit 2    "Beams" (behaviour of beams that prefer
//                                   the enemy ship over fighters, as well as
//                                   fighters moving too fast to place all
//                                   their strikes, PHost 4.0k+)
//                          Bit 15   Valid bit. If zero, the value of this
//                                   word is undefined and should be treated
//                                   as zero.
                    //if its ship-planet battle, determine where is the ship,
                    //left or right
                    if (vcr.battletype != 0)
                    {
                        //planet havent hullid                       
                          writer.Write((ushort)(vcr.left.hullid == 0 ? vcr.left.temperature : vcr.right.temperature));                        
                    }
                    else
                    {
                        //its ship-ship battle so dont care what will be in temperature field
                        writer.Write((ushort)0); 
                    }
                    
// +6     WORD    Type of battle
//                 0      Ship/Ship
//                 1      Ship/Planet
                    writer.Write((ushort)vcr.battletype);
// +8     WORD    Mass of left object
                    writer.Write((ushort)vcr.left.mass);
//+10     WORD    Mass of right object
                    writer.Write((ushort) vcr.right.mass);
//+12  42 BYTEs   Left object (Ship)
//                 +0  20 BYTEs   Name. Note that many PHost versions store
//                                this field as a zero-terminated string.
                    writer.Write(Utils.GetNormalizedShipName(vcr.left.name));
//                +20     WORD    Damage at beginning
                    writer.Write((ushort)vcr.left.damage);
//                +22     WORD    Crew
                    writer.Write((ushort)vcr.left.crew);
//                +24     WORD    Id Number
                    writer.Write((ushort)vcr.left.id);
//                +26     BYTE    Owner
                    writer.Write((byte)vcr.left.raceid);
//                +27     BYTE    Host: zero (Host addresses +26 as a WORD)
//                                PHost: race number, 0 if equal to owner,
//                                or in older PHosts. This is used to ensure
//                                that the VCR player knows the correct
//                                value of the `PlayerRace' option.
                    writer.Write((byte)0);
//                +28     BYTE    Picture. This field contains the picture
//                                number (RESOURCE.PLN) only, palette
//                                rotations as done by the Planets program
//                                are not reported. So the Alchemy Ships look
//                                exactly the same as a Super Transport
//                                Freighter. The Nebula Class Cruiser usually
//                                has picture #16. However, when it has
//                                Transwarps (type 9), THost assigns it #30
//                                for no apparent reason (easter egg?)
                    //todo:map vcr.left.hullid to proper image from resource.pln
                    //19 is random value.
                    writer.Write((byte)19);
//                +29     BYTE    Host: zero (Host addresses +28 as a WORD)
//                                PHost: hull number of ship, 0 for planets
//                                or in older PHosts.
                    writer.Write((byte) 0);
//                +30     WORD    Type of beams
                    writer.Write((ushort) vcr.left.beamid);
//                +32     BYTE    Number of beams
                    writer.Write((byte) vcr.left.beamcount);
//                +33     BYTE    Host: zero (Host addresses +32 as a WORD)
//                                PHost: experience level of object [PHost 4
//                                and later]
                    writer.Write((byte) 0);
//                +34     WORD    Fighter Bays
                    writer.Write((ushort) vcr.left.baycount);
//                +36     WORD    Torpedo Type
                    writer.Write((ushort) vcr.left.torpedoid);
//                +38     WORD    Number of Fighters/Torps
                    writer.Write((ushort) Math.Max(vcr.left.torpedos, vcr.left.fighters));
//                +40     WORD    Number of Torpedo Launchers
                    writer.Write((ushort) vcr.left.launchercount);
                    //todo: check it,may be launcherscount isnt torpedo launchers count
//                                See below for notes about this field.
//+54  42 BYTEs   Right object (Ship/Planet). The same format as for the left
//                one.
//                +0  20 BYTEs   Name. Note that many PHost versions store
//                                this field as a zero-terminated string.
                    writer.Write(Utils.GetNormalizedShipName(vcr.right.name));
//                +20     WORD    Damage at beginning
                    writer.Write((ushort)vcr.right.damage);
//                +22     WORD    Crew
                    writer.Write((ushort)vcr.right.crew);
//                +24     WORD    Id Number
                    writer.Write((ushort)vcr.right.id);
//                +26     BYTE    Owner
                    writer.Write((byte)vcr.right.raceid);
//                +27     BYTE    Host: zero (Host addresses +26 as a WORD)
//                                PHost: race number, 0 if equal to owner,
//                                or in older PHosts. This is used to ensure
//                                that the VCR player knows the correct
//                                value of the `PlayerRace' option.
                    writer.Write((byte)0);
//                +28     BYTE    Picture. This field contains the picture
//                                number (RESOURCE.PLN) only, palette
//                                rotations as done by the Planets program
//                                are not reported. So the Alchemy Ships look
//                                exactly the same as a Super Transport
//                                Freighter. The Nebula Class Cruiser usually
//                                has picture #16. However, when it has
//                                Transwarps (type 9), THost assigns it #30
//                                for no apparent reason (easter egg?)
                    //todo:map vcr.right.hullid to proper image from resource.pln
                    //19 is random value.
                    writer.Write((byte)19);
//                +29     BYTE    Host: zero (Host addresses +28 as a WORD)
//                                PHost: hull number of ship, 0 for planets
//                                or in older PHosts.
                    writer.Write((byte) 0);
//                +30     WORD    Type of beams
                    writer.Write((ushort) vcr.right.beamid);
//                +32     BYTE    Number of beams
                    writer.Write((byte) vcr.right.beamcount);
//                +33     BYTE    Host: zero (Host addresses +32 as a WORD)
//                                PHost: experience level of object [PHost 4
//                                and later]
                    writer.Write((byte) 0);
//                +34     WORD    Fighter Bays
                    writer.Write((ushort) vcr.right.baycount);
//                +36     WORD    Torpedo Type
                    writer.Write((ushort) vcr.right.torpedoid);
//                +38     WORD    Number of Fighters/Torps
                    writer.Write((ushort) Math.Max(vcr.right.torpedos, vcr.right.fighters));
//                +40     WORD    Number of Torpedo Launchers
                    writer.Write((ushort) vcr.right.launchercount);
                    //todo: check it,may be launcherscount isnt torpedo launchers count
//+96     WORD    Shields of left object
                    writer.Write((ushort) vcr.left.shield);
//+98     WORD    Shields of right object
                    writer.Write((ushort) vcr.right.shield);
                }
            }
        }
    }
}
