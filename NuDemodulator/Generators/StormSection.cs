﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;

namespace NuDemodulator.Generators
{
    class StormSection
    {
        public static void GenerateStormSection(List<Ionstorm> storms, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                var presentStorms = storms.ToDictionary(minefield => minefield.id, minefield => minefield);

                for (int i = 0; i < 50; i++)
                {
                    //ship with id == i not present in our list
                    if (!presentStorms.ContainsKey(i))
                    {
                        //write 12 bytes of pure zeroes
                        writer.Write((ulong)0);
                        writer.Write((ushort)0);
                        continue;
                    }
                //  +? 600 BYTEs   Ion storms, 50 records of 12 bytes each
                // +0     WORD    X
                    writer.Write((ushort)presentStorms[i].x);
                // +2     WORD    Y. Note that THost sometimes generates
                //                storms with negative coordinates.
                    writer.Write((ushort)presentStorms[i].y);
                // +4     WORD    Radius
                    writer.Write((ushort)presentStorms[i].radius);
                // +6     WORD    Voltage in MeV
                    writer.Write((ushort)presentStorms[i].voltage);
                //                 0      non-existent
                //                 even   weakening storm
                //                 odd    growing storm
                // +8     WORD    Warp
                    writer.Write((ushort)presentStorms[i].warp);
                //+10     WORD    Heading in degrees
                    writer.Write((ushort)presentStorms[i].heading);
                }
            }
        }
    }
}
