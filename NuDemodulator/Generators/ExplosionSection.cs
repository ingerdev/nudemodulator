﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;
using NuDemodulator.Datatypes;

namespace NuDemodulator.Generators
{
    public struct Explosion
    {        
        public int X;
        public int Y;
        public String ShipName;
    }

    class ExplosionSection
    {
        
        public static void GenerateExplosionSection(List<Message> messages, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                var explosions = getExplosionsFromMessages(messages).ToList();

                for (int i = 0; i < 50; i++)
                {
                    //+?  50 RECORDs of 4 bytes each: Explosions
                    if (i < explosions.Count())
                    {
                        // +0     WORD    X (0 = non-existent)                        
                        writer.Write((ushort)explosions[i].X);
                        // +2     WORD    Y    
                        writer.Write((ushort)explosions[i].Y);
                        //THost generates an explosion entry for every ship that
                        //exploded due to excess damage in battle or after a mine
                        //hit. PHost only generates these entries for ships
                        //exploding after a mine hit.
                    }
                    else
                    {
                        //write 4 bytes of pure zeroes
                        writer.Write((int)0);
                    }
                    
                        
                        
                        
                    
                }


            }
        }

        //we havent any explosions source except message log
        //so lets take them from message log
        private static IEnumerable<Explosion> getExplosionsFromMessages(IEnumerable<Message> messages)
        {
            var eMessages = messages.Where(message => message.messagetype == (int) Message.MessageType.Explosion);

            foreach (var msg in eMessages)
            {
                if (!msg.body.Contains("Distress call and explosion detected at ("))
                {
                    throw new InvalidDataException(
                        String.Format(
                            "Message have type field of explosion but not match explosion text template: {0}", msg.body));
                }
                yield return new Explosion(){X = msg.x,Y=msg.y,};
            }       
        }
    }
}
