﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuDemodulator.Generators
{

    //we havent any related structure in the planets.nu
    // so lets just skip it
    
    class Ufo
    {

        public static void GenerateUfoHSTFile(int raceid,string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream, Encoding.ASCII, false))
                {
                    GenerateUfoSection(raceid, fileStream);
                }
            }
            //System.IO.File.WriteAllLines(fileName,  from PlanetName planet in planets select planet.Name);
        }

        public static void GenerateUfoSection( int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {              
                byte[] zeroBuffer = new byte[7800];
                writer.Write(zeroBuffer);
            }
        }
    }
}
