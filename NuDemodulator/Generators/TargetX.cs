﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    public class TargetX
    {
        public static void GenerateTargetXFile(List<Ship> ships, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    //ship array contain all ship list, our and alien
                    //so we should filter them
                                       
                    GenerateTargetXSection(ships, raceid, fileStream);

                    // +m  10 BYTEs   Signature 2
                    writer.Write(SignaturesUtil.encodePassword(Constants.DEFAULT_PASSWORD));
                }

            }
        }

        //write values as they should be in proper rst section
        public static void GenerateTargetXSection(List<Ship> ships, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream,Encoding.ASCII,true))
            {
                var alienships = ships.Where(ship => ship.ownerid != raceid).ToList();
                // +0     WORD    Number of ships
                writer.Write((ushort)alienships.Count());

                //we should write no more than MAX_TARGETS_COUNT (they usually equal 50),
                //old packers need no more than 50 visual scan infos3
                foreach (var ship in alienships.Take(Constants.MAX_TARGETS_COUNT))
                {
                    writeShipData(ship, writer);
                }
            }
        }

        //write rest of ship contacts beyond Constants.MAX_TARGETS_COUNT numbers
        public static void GenerateAdditionalTargetXSection(List<Ship> ships, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                var alienships = ships.Where(ship => ship.ownerid != raceid).ToList();
                // +0     WORD    Number of ships
                writer.Write((ushort)alienships.Count() - Constants.MAX_TARGETS_COUNT);

                //we should write no more than MAX_TARGETS_COUNT (they usually equal 50),
                //old packers need no more than 50 visual scan infos3
                foreach (var ship in alienships.Skip(Constants.MAX_TARGETS_COUNT))
                {
                    writeShipData(ship, writer);
                }
            }
        }


        //default Targetx contain no more than 50 contacts
        //so rest of them should be added to rst file 
        public static bool IsAdditionalContactsPresent(List<Ship> ships, int raceid)
        {
            return ships.Where(ship => ship.ownerid != raceid).ToList().Count>50;
        }

        private static void writeShipData(Ship ship, BinaryWriter writer)
        {
            //                       A ship record:
            // +0     WORD    Id Number
            writer.Write((ushort)ship.id);
            // +2     WORD    Owner
            writer.Write((ushort)ship.ownerid);
            // +4     WORD    Warp (zero if ship doesn't have fuel, no matter what its
            //                real speed is)
            writer.Write((ushort)ship.warp);
            // +6     WORD    X position
            writer.Write((ushort)ship.x);
            // +8     WORD    Y position
            writer.Write((ushort)ship.y);
            //+10     WORD    Type of starship hull (might differ from real ship type
            //                if the RacePlus Chameleon Device is used)
            writer.Write((ushort)ship.hullid);
            //+12     WORD    Heading in degrees, 0=North, 90=East, 180=South, 270=West,
            //                -1=unknown (i.e., not moving)
            writer.Write((ushort)ship.heading);
            //+14  20 BYTEs   Ship name.
            writer.Write(Utils.GetNormalizedShipName(ship.name));
        }
    }
}