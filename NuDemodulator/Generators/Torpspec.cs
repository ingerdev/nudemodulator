﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    class Torpspec
    {
        public static void GenerateTorpspecFile(List<Torpedo> torps, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    GenerateTorpspecSection(torps, raceid, fileStream);
                    //The torpedo records are followed by 90 unused bytes. 
                    writer.Write(new byte[90]);
                }
            }
        }

        private static void GenerateTorpspecSection(List<Torpedo> torps, int raceid, FileStream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                foreach (var torp in torps)
                {
                    //                 +0  20 BYTEs   Name
                    writer.Write(Utils.GetNormalizedTorpedoName(torp.name));
//                    +20     WORD    Cost (mc for a Torpedo)
                    writer.Write((ushort)torp.torpedocost);
//+22     WORD    Cost (mc for a Launcher)
                    writer.Write((ushort)torp.launchercost);
//+24     WORD    Tritanium needed for Launcher
                    writer.Write((ushort)torp.tritanium);
//+26     WORD    Duranium needed for Launcher
                    writer.Write((ushort)torp.duranium);
//+28     WORD    Molybdenum needed for Launcher
                    writer.Write((ushort)torp.molybdenum);
//+30     WORD    Mass of Launcher
                    writer.Write((ushort)torp.mass);
//+32     WORD    Tech Level
                    writer.Write((ushort)torp.techlevel);
//+34     WORD    Kill Value
                    writer.Write((ushort)torp.crewkill);
//+36     WORD    Damage Value
                    writer.Write((ushort)torp.damage);
                }
            }
        }
    }
}
