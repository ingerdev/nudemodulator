﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    public class BDataX
    {
        public static void GenerateBDataXFile(List<Starbas> bases,List<Stock> stocks,List<Hull> hulls, List<Race> races, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {

                    GenerateBDataXSection(bases,stocks, raceid, fileStream);

                    // +m  10 BYTEs   Signature 2
                    writer.Write(SignaturesUtil.encodePassword(Constants.DEFAULT_PASSWORD));
                }

            }
        }

        //todo: UNFINISHED YET. Starbase parts are written in the Stock objects in unclear format
        //write values as they should be in proper rst section
        public static void GenerateBDataXSection(List<Starbas> bases,List<Stock> stocks,List<Hull> hulls, List<Race> races, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
              //+0     WORD    Number of starbases
              writer.Write(bases.Count);
                foreach(var starbase in bases)
                {
//                   One Starbase Record:
// +0     WORD    Base Id (=Id of planet with this base)
                    writer.Write((ushort)starbase.planetid);
// +2     WORD    Owner (1..11, BDATA.HST: 0=no base at this planet)
                    //planets.nu: our starbases always have raceid:0
                    writer.Write((ushort) (starbase.raceid == 0 ? raceid : starbase.raceid));
// +4     WORD    Defense (0..200)
//                One starbase defense unit costs 10 mc and 1 Duranium.
                    writer.Write((ushort) starbase.defense);
// +6     WORD    Damage (0..100)
                    writer.Write((ushort) starbase.damage);
// +8     WORD    Engine Tech Level (1..10)
                    writer.Write((ushort) starbase.enginetechlevel);
//+10     WORD    Hulls Tech Level (1..10)
                    writer.Write((ushort) starbase.hulltechlevel);
//+12     WORD    Weapon Tech Level (1..10)
                    writer.Write((ushort) starbase.beamtechlevel);
//+14     WORD    Torpedo Tech Level (1..10)
//                Tech upgrades: going from Tech N to N+1 costs 100*N mc.
                    writer.Write((ushort) starbase.torptechlevel);
//+16   9 WORDs   Engines in storage (for the 9 engines from ENGSPEC.DAT)
                    writer.Write(getEnginesInStorage(starbase,stocks));
                   // writer.Write((ushort)starbase.
//+34  20 WORDs   Hulls in Storage. Each race has up to 20 possible hulls it
//                can build. These 20 words correspond to the 20 TRUEHULL.DAT
//                slots. There's no possibility to store hulls a player can't
//                build normally.
                    //RaceHulls raceHulls= new RaceHulls(hulls,races,raceid);
                    var originalHulls = Hullspec.GetOriginalHulls(hulls);
                    for (int k = 0; k < 20; k++)
                    {
                        writer.Write();
                    }
//+74  10 WORDs   Beam Weapons in storage
//+94  10 WORDs   Torpedo Launchers in storage
//+114 10 WORDs   Torpedoes in storage
//                All the "storage" fields can hold values between 0 and 10000.
//                As of Host 3.22.031, these fields are not allowed to be
//                increased without sufficient tech. This prevents the cheat
//                of building parts without tech, but also prevents the (legal)
//                act of moving high-tech torps from a ship to a low-tech base.
//+134    WORD    Number of fighters (0..60)
//+136    WORD    Id of ship to be recycled/repaired
//+138    WORD    What to do with that ship:
//                 0      Nothing
//                 1      Fix
//                 2      Recycle
//+140    WORD    Mission (Primary Order)
//                 0      none
//                 1      Refuel
//                 2      Max Defense
//                 3      Load Torps onto ships
//                 4      Unload freighters
//                 5      Repair base
//                 6      Force a surrender
//+142    WORD    Type of ship to build (0=no build order, 1..20=index into
//                above array)
//+144    WORD    Engine Type
//+146    WORD    Beam Type
//+148    WORD    Beam Count
//+150    WORD    Torpedo Type
//+152    WORD    Torpedo Count
//+154    WORD    0. According to CPLAYER.BAS, this is a fighter count. When
//                building a ship this value is effectively ignored, that's
//                why PLANETS.EXE puts zero in here. This field should
//                _really_ always be zero, after the build as many fighters
//                as shown here are destroyed with no compensation...

                }
            }
        }

        private static byte[] getEnginesInStorage(Starbas starbase,List<Stock> stocks)
        {
           // - geat all parts related to starbase
            var parts = stocks.Where(stock => stock.starbaseid == starbase.id);
            return null;
        }

        private static byte[] getHullsInStorage(Starbas starbase, List<Stock> stocks, List<Race> races, int raceid)
        {
           //get racehulls

            return null;
        }
    }
}
