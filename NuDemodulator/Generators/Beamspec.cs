﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    class Beamspec
    {
        public static void GenerateHullspecFile(List<Beam> beams, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    GenerateBeamspecSection(beams, raceid, fileStream);
                }
            }
        }

        private static void GenerateBeamspecSection(List<Beam> beams, int raceid, FileStream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {               
                foreach (var beam in beams)
                {
//                 +0  20 BYTEs   Name
                    writer.Write(Utils.GetNormalizedBeamName(beam.name));
//+20     WORD    Cost (mc)
                    writer.Write((ushort)beam.cost);
//+22     WORD    Tritanium needed
                    writer.Write((ushort)beam.tritanium);
//+24     WORD    Duranium needed
                    writer.Write((ushort)beam.duranium);
//+26     WORD    Molybdenum needed
                    writer.Write((ushort)beam.molybdenum);
//+28     WORD    Mass
                    writer.Write((ushort)beam.mass);
//+30     WORD    Tech Level
                    writer.Write((ushort)beam.techlevel);
//+32     WORD    Kill value
                    writer.Write((ushort)beam.crewkill);
//+34     WORD    Damage value
                    writer.Write((ushort)beam.damage);

                }
            }
        }
    }
}
