﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    public class RaceHulls
    {
      

        /// <summary>
        /// Getting hull index in 20-row index table
        /// </summary>
        /// <param name="hullId">
        /// hull id (global hull id from planets.nu hulls)
        /// </param>
        public int GetHullTableIndexById(int hullId)
        {
            Hull hull = _hullMap.Keys.FirstOrDefault(h => h.id == hullId);
            if (hull == null)
                throw new ArgumentException(
                    String.Format("RaceHulls.GetHullTableIndexById: no hull with id {0} belong to race", hullId));

            return _hullMap[hull];
        }


        //For each player (11) an array with 20 WORDs. Each WORD contains the
        //number of a hull that the player can build (Index into HULLSPEC.DAT).
        //These 20 "hull slots" correspond to the appropriate field in
        //the starbase record.
        public void SerializeHulls(BinaryWriter writer)
        {
            //invert dictionary
            var hulls = _hullMap.ToDictionary(x => x.Value, x => x.Key);

            //When a race can build fewer than 20 ship types, the last fields should
            //be zero; there should not be zeroes between filled ship slots.
            for (int k = 0; k < Constants.SHIP_LIST_LENGTH_MAX; k++)
            {
                if (hulls.ContainsKey(k))
                {
                    writer.Write((ushort) _originalHulls.FindIndex(hull => hull.id == hulls[k].id));
                }
                else
                {
                    writer.Write((ushort) 0);
                }
            }
        }
    }

    internal class TrueHull
    {
        public static void GenerateTrueHullFile(List<Hull> hulls, List<Race> races, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    GenerateTrueHullSection(hulls, races, raceid, fileStream);
                }
            }
        }

        private static void GenerateTrueHullSection(List<Hull> hulls, List<Race> races, int raceid,
            FileStream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                var originalHulls = Hullspec.GetOriginalHulls(hulls);
                //new ships on planets.nu have parentid!=0
                foreach (var race in RaceNM.FilterRaces(races))
                {
                    var race = races.First(rce => rce.id == raceid);
                    var raceHulls = 
                    
                    raceHulls.SerializeHulls(writer);
                }
            }
        }
    }
}