﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    public class ShipXYX
    {
        public static void GenerateShipXYXFile(List<Ship> ships, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream, Encoding.ASCII, true))
                {
                    //ship array contain all ship list, our and alien
                    //so we should filter them

                    GenerateShipXYXSection(ships, raceid, fileStream);

                    // +m  10 BYTEs   Signature 2
                    writer.Write(SignaturesUtil.encodePassword(Constants.DEFAULT_PASSWORD));
                }

            }
        }

        //write values as they should be in proper rst section
        public static void GenerateShipXYXSection(List<Ship> ships, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                //note: shipXY should contain data for 500 ships(!)
                //planets.nu have data only from visible ships
                Dictionary<int, Ship> visibleShips = ships.ToDictionary(ship => ship.id, ship => ship);
                for (int i = 0; i < 500; i++)
                {
                    //ship with id == i not present in our list
                    if (!visibleShips.ContainsKey(i))
                    {
                        //write 8 bytes of pure zeroes
                        writer.Write((ulong) 0);
                        continue;
                    }

                    //A ship record:
                    // +0     WORD    X position
                    writer.Write((ushort)visibleShips[i].x);
                    // +2     WORD    Y position
                    writer.Write((ushort)visibleShips[i].y);
                    // +4     WORD    Owner
                    writer.Write((ushort)visibleShips[i].ownerid);
                    // +6     WORD    Mass in kt
                    writer.Write((ushort)visibleShips[i].mass);

                }


            }
        }
    }
}
