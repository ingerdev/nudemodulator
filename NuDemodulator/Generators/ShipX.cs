﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    class ShipX
    {
        private enum ShipTransferType { Planet=1,Ship=2}
             
        public static void GenerateShipXFile(List<Ship> ships,int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    GenerateShipXSection(ships, raceid, fileStream);
                    // +m  10 BYTEs   SHIPx.DAT: Signature 2
                    writer.Write(SignaturesUtil.encodePassword(Constants.DEFAULT_PASSWORD));
                }
            }
            //System.IO.File.WriteAllLines(fileName,  from PlanetName planet in planets select planet.Name);
        }
   
        public static void GenerateShipXSection(List<Ship> ships, int raceid, Stream outputStream)
        {
            using (var writer = new BinaryWriter(outputStream, Encoding.ASCII, true))
            {
                var ownAliveShips = ships.Where(ship => ship.ownerid == raceid).ToList();
                //+0     WORD    Number of ships
                writer.Write((ushort)ownAliveShips.Count());
                //ship array contain all visible ships list, ours and alien
                //so we should filter them
                foreach (var ship in ownAliveShips)
                {
                    //note: planets.nu keeps dead ships in ship list
                    //with crew = -1 and/or damage = -1
                    //even more interesting, our ships that was killed by another race
                    //will have their race ownerid (so we dont write them here, instead
                    // they will be written in targetx.dat)
                    //                        A ship record:
                    // +0     WORD    Ship Id
                    writer.Write((ushort)ship.id);

                    // +2     WORD    Player Id (1..11, or zero in SHIP.HST for free ship slots)
                    writer.Write((ushort)ship.ownerid);
                    // +4   3 BYTEs   Friendly Code. THost permits ASCII codes 32 to 122.
                    //dead ships have fc "" so we should extend it at least up to 3 symbols
                    string fc = ship.friendlycode.PadRight(3);
                    writer.Write(Convert.ToByte(fc[0]));
                    writer.Write(Convert.ToByte(fc[1]));
                    writer.Write(Convert.ToByte(fc[2]));
                    // +7     WORD    Warp factor (0..9)
                    writer.Write((ushort)ship.warp);
                    //                Sometimes this field is set to a negative value by the
                    //                host, if the ship has more than 100% damage (due to blindly
                    //                applying the max-speed-for-damaged-ship formula).
                    // +9     WORD    X distance to waypoint (destination-X minus X-position)
                    writer.Write((short)(ship.targetx - ship.x));
                    //+11     WORD    Y distance to waypoint (destination-Y minus Y-position)
                    //                The distances must be in range -3000 .. +3000
                    writer.Write((short)(ship.targety - ship.y));
                    //+13     WORD    X position
                    writer.Write((short)ship.x);
                    //+15     WORD    Y position
                    writer.Write((short)ship.y);
                    //                Usually between 1 and 4000 or 1 and 10000. Ships with (X,Y)
                    //                = (0,0) should not appear, as this makes problems with the
                    //                SHIPXYx.DAT file and probably many clients.
                    //+17     WORD    Engine type (1..9, index into ENGSPEC.DAT)
                    writer.Write((ushort)ship.engineid);
                    //+19     WORD    Hull type (1..105, index into HULLSPEC.DAT)
                    writer.Write((ushort)ship.hullid);
                    //+21     WORD    Beam weapon type (0 for none, or 1..10 = index into
                    //                BEAMSPEC.DAT). See note below.
                    writer.Write((ushort)ship.beamid);
                    //+23     WORD    Number of beams
                    writer.Write((ushort)ship.beams);
                    //+25     WORD    Number of fighter bays (if not zero, the T-Launcher type
                    //                and T-Launcher number values are ignored/set to 0, since a
                    //                ship can never have Torpedoes and Fighters)
                    writer.Write((ushort)ship.bays);
                    //+27     WORD    Torpedo Launcher Type (0 for none, 1..10 as index into
                    //                TORPSPEC.DAT). See note below.
                    writer.Write((ushort)ship.torpedoid);
                    //+29     WORD    Number of Torpedoes/Fighters
                    writer.Write((ushort)ship.ammo);
                    //+31     WORD    Number of Torpedo launchers
                    writer.Write((ushort)ship.torps);
                    //+33     WORD    Mission. See below.
                    writer.Write((ushort)ship.mission);
                    //+35     WORD    Primary Enemy (0 or race Id 1..11)
                    writer.Write((ushort)ship.enemy);
                    //+37     WORD    Id of ship to tow, or first mission argument
                    writer.Write((ushort)ship.mission1target);
                    //+39     WORD    Damage % (0..149)
                    writer.Write((ushort)ship.damage);
                    //+41     WORD    Crew
                    writer.Write((ushort)ship.crew);
                    //+43     WORD    Colonist Clans
                    writer.Write((ushort)ship.clans);
                    //+45  20 BYTEs   Name
                    writer.Write(Utils.GetNormalizedShipName(ship.name));
                    //+65     WORD    Neutronium
                    writer.Write((ushort)ship.neutronium);
                    //+67     WORD    Tritanium
                    writer.Write((ushort)ship.tritanium);
                    //+69     WORD    Duranium
                    writer.Write((ushort)ship.duranium);
                    //+71     WORD    Molybdenum
                    writer.Write((ushort)ship.molybdenum);
                    //+73     WORD    Supplies
                    writer.Write((ushort)ship.supplies);

                    //planets.nu can transfer only to enemy planet OR enemy ship
                    //thus, we should zeroing fields that are not used

                    //+75   7 WORDs   Unload Cargo to planet
                    bool transferToPlanet = (ship.transfertargettype == (int)ShipTransferType.Planet);
                    //                +75     WORD    Neutronium
                    writer.Write((ushort)(transferToPlanet ? ship.transferneutronium : 0));
                    //                +77     WORD    Tritanium
                    writer.Write((ushort)(transferToPlanet ? ship.transfertritanium : 0));
                    //                +79     WORD    Duranium
                    writer.Write((ushort)(transferToPlanet ? ship.transferduranium : 0));
                    //                +81     WORD    Molybdenum
                    writer.Write((ushort)(transferToPlanet ? ship.transfermolybdenum : 0));
                    //                +83     WORD    Colonists
                    writer.Write((ushort)(transferToPlanet ? ship.transferclans : 0));
                    //                +85     WORD    Supplies
                    writer.Write((ushort)(transferToPlanet ? ship.transfersupplies : 0));
                    //                +87     WORD    Planet Id or 0 for Jettison
                    writer.Write((ushort)(transferToPlanet ? ship.transfertargetid : 0));
                    //                (see text below!)
                    //+89   7 WORDs   Transfer to enemy ship
                    //                  +75     WORD    Neutronium
                    writer.Write((ushort)(!transferToPlanet ? ship.transferneutronium : 0));
                    //                +77     WORD    Tritanium
                    writer.Write((ushort)(!transferToPlanet ? ship.transfertritanium : 0));
                    //                +79     WORD    Duranium
                    writer.Write((ushort)(!transferToPlanet ? ship.transferduranium : 0));
                    //                +81     WORD    Molybdenum
                    writer.Write((ushort)(!transferToPlanet ? ship.transfermolybdenum : 0));
                    //                +83     WORD    Colonists
                    writer.Write((ushort)(!transferToPlanet ? ship.transferclans : 0));
                    //                +85     WORD    Supplies
                    writer.Write((ushort)(!transferToPlanet ? ship.transfersupplies : 0));
                    //                +87     WORD    Planet Id or 0 for Jettison
                    writer.Write((ushort)(!transferToPlanet ? ship.transfertargetid : 0));
                    //                (see text below!)
                    //+103    WORD    Id number for intercept mission or second mission argument.
                    //                In THost, this field must contain a valid ship Id or zero,
                    //                even if 3rd-party missions are used.
                    writer.Write((ushort)(ship.mission2target));
                    //+105    WORD    Money (0..10000) 
                    writer.Write((ushort)(ship.megacredits));


                }
            }
        }
    
    }
}
