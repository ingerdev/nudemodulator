﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{


    //creates Planet.nm file
    class PlanetNM
    {
        public static List<byte[]> GeneratePlanetsNM(List<Planet> planets )
        {

            return planets.Select(planet => Utils.GetNormalizedPlanetName(planet.name)).ToList();
        }

        public static void GeneratePlanetsNMFile(List<Planet> planets, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream, Encoding.ASCII, false))
                {
                    foreach (var name in GeneratePlanetsNM(planets))
                    {
                        writer.Write(name);
                    }
                }
            }
            //System.IO.File.WriteAllLines(fileName,  from PlanetName planet in planets select planet.Name);
        }


        //x-coordinate, y-coordinate, owner
        public static List<Tuple<ushort, ushort, ushort>> GenerateXYPlan(List<Planet> planets)
        {
            return
                planets.Select(
                    planet =>
                        new Tuple<ushort, ushort, ushort>((ushort)planet.x, (ushort)planet.y, (ushort)planet.ownerid)).ToList();
        }

        public static void GenerateXYPlanFile(List<Planet> planets, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    foreach (var values in GenerateXYPlan(planets))
                    {
                        writer.Write(values.Item1);
                        writer.Write(values.Item2);
                        writer.Write(values.Item3);
                    }
                }
            }
            //System.IO.File.WriteAllLines(fileName,  from PlanetName planet in planets select planet.Name);
        }
    }
}
