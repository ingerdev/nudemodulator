﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    class MdataX
    {     
        public static void GenerateMDataXSection(List<Message> messages, int raceid, Stream outputStream)
        {
            using (var writer = new BinaryWriter(outputStream, Encoding.ASCII, true))
            {
                //            +0     WORD    Number of messages
                writer.Write(messages.Count);

                List<byte[]> encryptedMessages = messages.Select(msg => encryptMessage(msg.body)).ToList();
                
                int originPos = (int) outputStream.Position;
                int headerSize = encryptedMessages.Count * 6;
                int messagesSize = 0;

                //+2 n*6 BYTEs   Records of 6 bytes each
                //                +0     DWORD   Address of message in file + 1
                //                +4     WORD    Length of message in bytes              
                foreach (var msg in encryptedMessages)
                {
                    writer.Write((int) (originPos + headerSize + messagesSize +1));
                    writer.Write((ushort) msg.Length);
                    messagesSize += msg.Length;
                }

                //+x   y BYTEs   undefined, or the messages
                foreach (var msg in encryptedMessages)
                {
                    writer.Write(msg);
                }
            }
        }

        private static byte[] encryptMessage(String message)
        {
  //              The messages texts are encrypted: each ASCII code is increased by 13dec.
  //A single carriage return (ASCII 0Dh, encrypted 1Ah) is a line break.
  //Empty lines are stored as a line break only (no spaces). Messages do not
  //need to be all of the same length. Messages from Winplan clients may have
  //additional encrypted linefeeds (ASCII 0Ah, encrypted 17h) after the CRs.

  //Characters with an ASCII code above 242 can't be encrypted in messages.
  //The encryption does not have a "wraparound" (then, 250 + 13 would be 7).
  //Attention: Planets doesn't check this and exits with an "Illegal
  //Function Call" message. Host knows about this problem and replaces
  //the characters with ASCII 13h (encrypted 20h = " ").

            var str = Utils.GetASCIIString(message);

            for(int k=0; k<str.Length; k++)
            {
                str[k] = str[k] < 242 ? (byte)(str[k] + 0x0D) : (byte)0x20;
            }

            return str;

        }
    }
}
