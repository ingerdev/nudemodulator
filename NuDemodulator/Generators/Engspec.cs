﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    class Engspec
    {
        public static void GenerateHullspecFile(List<Engine> engines, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    GenerateEngspecSection(engines, raceid, fileStream);
                     //ENGSPEC.DAT         598 bytes (4 bytes more than required)
                    writer.Write((int)0);
                }
            }
        }

        private static void GenerateEngspecSection(List<Engine> engines,  int raceid, FileStream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {                
                //new ships on planets.nu have parentid!=0
                foreach (var engine in engines)
                {
//                  For each engine (9) a record of 66 bytes:
// +0  20 BYTEs   Name
                    writer.Write(Utils.GetNormalizedEngineName(engine.name));
//+20     WORD    Cost (mc)
                    writer.Write((ushort) engine.cost);
//+22     WORD    Tritanium needed
                    writer.Write((ushort)engine.tritanium);
//+24     WORD    Duranium needed
                    writer.Write((ushort)engine.duranium);
//+26     WORD    Molybdenum needed
                    writer.Write((ushort)engine.molybdenum);
//+28     WORD    Tech Level
                    writer.Write((ushort)engine.techlevel);
//+30   9 DWORDs  Fuel used to travel 1 month at the given speed, for each
//                warp factor, for a 100000 kt ship
                    writer.Write((uint)engine.warp1);
                    writer.Write((uint)engine.warp2);
                    writer.Write((uint)engine.warp3);
                    writer.Write((uint)engine.warp4);
                    writer.Write((uint)engine.warp5);
                    writer.Write((uint)engine.warp6);
                    writer.Write((uint)engine.warp7);
                    writer.Write((uint)engine.warp8);
                    writer.Write((uint)engine.warp9);
                   
                }
            }
        }

    }
}
