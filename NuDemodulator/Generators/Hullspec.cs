﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    class Hullspec
    {
        public static void GenerateHullspecFile(List<Hull> hulls, int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {
                    GenerateHullspecSection(hulls, raceid, fileStream);
                }

            }
        }

        public static void GenerateHullspecSection(List<Hull> hulls, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                //original ships in planets.nu have isbase = true
                var originalHulls = GetOriginalHulls(hulls);

                Debug.Assert(originalHulls.Count == 105);

                foreach (var hull in originalHulls)
                {

//            For each hull (105) a record of 60 bytes:

// +0  30 BYTEs   Name
                    writer.Write(Utils.GetNormalizedHullName(hull.name));
//+30     WORD    Number of picture in RESOURCE.PLN
//                Some pictures are used more than once, but with a different
//                palette (e.g., Super Transport Freighter + Merlin Class
//                Alchemy Ship + Neutronic Refinery Ship). I don't know where
//                this information comes from, it seems to be hard-coded in
//                PLANETS.EXE. Winplan automatically uses different pictures
//                for hull numbers 104 and 105 (see below).
                    //todo: map it to real images
                    writer.Write((ushort) 0);
//+32     WORD    =1. In VPCPLAY.BAS this field is identified as "image of a
//                heavily damaged ship", though it seems to be unused.
                    writer.Write((ushort) 1);
//+34     WORD    Tritanium needed
                    writer.Write((ushort) hull.tritanium);
//+36     WORD    Duranium needed
                    writer.Write((ushort)hull.duranium);
//+38     WORD    Molybdenum needed
                    writer.Write((ushort)hull.molybdenum);
//+40     WORD    Fuel tank size
                    writer.Write((ushort)hull.fueltank);
//+42     WORD    Crew size
                    writer.Write((ushort)hull.crew);
//+44     WORD    Number of engines
                    writer.Write((ushort)hull.engines);
//+46     WORD    Hull mass (empty)
                    writer.Write((ushort)hull.mass);
//+48     WORD    Tech Level
                    writer.Write((ushort)hull.techlevel);
//+50     WORD    Cargo room
                    writer.Write((ushort)hull.cargo);
//+52     WORD    Number of fighter bays
                    writer.Write((ushort)hull.fighterbays);
//+54     WORD    max. Number of torpedo launchers
                    writer.Write((ushort)hull.launchers);
//+56     WORD    max. Number of beam weapons
                    writer.Write((ushort)hull.beams);
//+58     WORD    Cost (mc)
                    writer.Write((ushort)hull.cost);
                }
            }
        }

        static public List<Hull> GetOriginalHulls(List<Hull> hulls)
        {
            return  hulls.Where(hull => hull.isbase).Take(Constants.SHIP_LIST_LENGTH_MAX).ToList();
        }
    }
}
