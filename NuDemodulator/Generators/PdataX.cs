﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Annotations;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{

    //this file should contain only planets i have or i orbiting
    class PdataX
    {
        public static void GeneratePdataXFile(List<Planet> planets, List<Ship> ships,int raceid, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {                 
                    //writer.Write(Utils.GetTimestamp(DateTime.Now));
                    GeneratePdataXSection(planets,ships, raceid, fileStream);
                    // +m  10 BYTEs   Signature 2
                    writer.Write(SignaturesUtil.encodePassword(Constants.DEFAULT_PASSWORD));
                }

            }
        }

        //write values as they should be in proper rst section
        public static void GeneratePdataXSection(List<Planet> planets,List<Ship> ships, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                //planets array have full set of 500 planets
                // so kind of filtering is necessary
                //filter all  planets by 
                //a) it should be mine
                //or
                //b) any my ship should be in the same XY point
                

                
                var ourPlanets = planets.Where(planet => planet.ownerid == raceid);
                var orbitingPlanets = getPlanetsWithOwnShipOrbiting(planets, ships, raceid);
              
                var resultPlanets = orbitingPlanets.Union(ourPlanets).ToList();

                // +0     WORD    Number of ships
                writer.Write((ushort)resultPlanets.Count());

                
                foreach (var planet in resultPlanets)
                {
//                  One Planet record:
// +0     WORD    Player Id, 0 for unowned planets
                    writer.Write((ushort) planet.ownerid);
// +2     WORD    Planet Id
                    writer.Write((ushort)planet.id);
// +4   3 BYTEs   Friendly Code. THost permits ASCII codes 32 to 122.
                    writer.Write((ushort) planet.friendlycode[0]);
                    writer.Write((ushort)planet.friendlycode[1]);
                    writer.Write((ushort) planet.friendlycode[2]);
// +7     WORD    Number of mines (0..~516)
                    writer.Write((ushort) planet.mines);
// +9     WORD    Number of factories (0..~416)
                    writer.Write((ushort) planet.factories);
//+11     WORD    Number of defense posts (0..~366)
                    writer.Write((ushort) planet.defense);
//                The ranges are based upon the maximum population of 10
//                million colonists (100000 clans). The maximum number of
//                mines on a planet is (COLON = colonist clans):
//                  COLON                        if COLON <= 200
//                  ROUND(200+SQRT(COLON-200))   if COLON > 200
//                Replace 200 with 100 for factories, and 50 for defenses.
//                When building structures, the client program must subtract
//                the appropriate costs from the available resources (1
//                supply, and 4/3/10 mc for mines/factories/defenses).
//+13     DWORD   Mined Neutronium
                    writer.Write((uint) planet.neutronium);
//+17     DWORD   Mined Tritanium
                    writer.Write((uint) planet.tritanium);
//+21     DWORD   Mined Duranium
                    writer.Write((uint) planet.duranium);
//+25     DWORD   Mined Molybdenum
                    writer.Write((uint) planet.molybdenum);
//+29     DWORD   Colonist clans (1 clan = 100 people). The THost limit
//                is 100000 clans, the PHost limit is 250000 clans.
                    writer.Write((uint) planet.clans);
//+33     DWORD   Supplies
                    writer.Write((uint) planet.supplies);
//+37     DWORD   Megacredits
//                To sell supplies, remove the requested amount from the
//                Supplies field and add it to the Megacredits field.
                    writer.Write((uint) planet.megacredits);
//+41     DWORD   Neutronium in ground
                    writer.Write((uint) planet.groundneutronium);
//+45     DWORD   Tritanium in ground
                    writer.Write((uint) planet.groundtritanium);
//+49     DWORD   Duranium in ground
                    writer.Write((uint) planet.duranium);
//+53     DWORD   Molybdenum in ground
                    writer.Write((uint) planet.groundmolybdenum);
//                        >4999           abundant
//                        1200..4999      very common
//                        600..1199       common
//                        100..599        rare
//                        1..99           very rare
//                        0               none
//+57     WORD    Neutronium density
                    writer.Write((ushort)planet.densityneutronium);
//+59     WORD    Tritanium density
                    writer.Write((ushort)planet.densitytritanium);
//+61     WORD    Duranium density
                    writer.Write((ushort)planet.densityduranium);
//+63     WORD    Molybdenum density
                    writer.Write((ushort)planet.densitymolybdenum);
//                        70..100 large masses    "1 mine extracts 1 kt"
//                        40..69  concentrated    "2 mines extract 1 kt"
//                        30..39  dispersed       "3 mines extract 1 kt"
//                        10..29  scattered       "5 mines extract 1 kt"
//                        0..9    very scattered  "10 mines extract 1 kt"
//                The figures on the right are from the docs, the actual
//                amount of minerals extracted is
//                - THost:
//                  ERnd(ERnd(mines * density / 100) * miningrate / 100) * RF
//                - PHost:
//                  Trunc(Trunc(density * miningrate / 100) * RF * mines / 100)
//                where "miningrate" is the HConfig value, and RF it the
//                "reptile factor" (2 if Reptilian natives, 1 otherwise).
//+65     WORD    Colonist taxes (0..100)
                    writer.Write((ushort) planet.colonisttaxrate);
//+67     WORD    Native taxes (0..100)
//                Note that THost limits taxation: when hissing, the maximum
//                tax rate is 75%. Cyborgs can tax natives up to 20%.
                    writer.Write((ushort) planet.nativetaxrate);
//+69     WORD    Colonist happiness (-300..100)
                    writer.Write((ushort) planet.colonisthappypoints);
//+71     WORD    Native happiness (-300..100)
//                        90..100 happy
//                        70..89  calm
//                        50..69  unhappy
//                        40..49  very angry
//                        20..39  rioting
//                        <20     fighting
//                Tax collection possible for happiness>=30, population grows
//                if >=70.
                    writer.Write((ushort) planet.nativehappypoints);
//+73     WORD    Native Government (SPI = Socio Political Index)
//                        0       none              0%
//                        1       Anarchy          20%
//                        2       Pre-Tribal       40%
//                        3       Early-Tribal     60%
//                        4       Tribal           80%
//                        5       Feudal          100%
//                        6       Monarchy        120%
//                        7       Representative  140%
//                        8       Participatory   160%
//                        9       Unity           180%
                    writer.Write((ushort) planet.nativegovernment);
//+75     DWORD   Native clans (1 clan = 100 people). Maximum population
//                somewhere around 150000 clans.
                    writer.Write((uint) planet.nativeclans);
//+79     WORD    Native Race
//                        0       none
//                        1       Humanoid
//                        2       Bovinoid
//                        3       Reptilian
//                        4       Avian
//                        5       Amorphous
//                        6       Insectoid
//                        7       Amphibian
//                        8       Ghipsoldal
//                        9       Siliconoid
                    writer.Write((ushort) planet.nativetype);
//+81     WORD    Temperature (Temperature in Fahrenheit = 100 - this value)
//                        0..15   desert  (85..100°F)
//                        16..35  tropical (65..84°F)
//                        36..60  warm    (40..64°F)
//                        61..85  cool    (15..39°F)
//                        86..100 arctic  (0..14°F)
//                In case you wonder why we store `100-F': originally, this
//                was just a type code with no real-world equivalent. It was
//                changed to mean a temperature with Host 3.20 and Winplan.
                    writer.Write((ushort) planet.temp);
//+83     WORD    1=Build base, 0 otherwise. The client program must subtract
//                the starbase cost (900mc, 402T, 120D, 340M; configurable in
//                PHost 4.0k+) from the available resources when setting this
//                word to 1.
                    writer.Write((ushort) (planet.buildingstarbase?1:0));
                }
            }
        }

        //creates list with planets that have our ship orbiting at them
        private static List<Planet> getPlanetsWithOwnShipOrbiting(List<Planet> planets, List<Ship> ships,int raceid)
        {
            var xCoords = ships.Where(ship => ship.ownerid == raceid).Select(ship => ship.x).ToList();
            var yCoords = ships.Where(ship => ship.ownerid == raceid).Select(ship => ship.y).ToList();
            
            xCoords.Sort(); yCoords.Sort();

            return planets.Where(planet => xCoords.BinarySearch(planet.x) > 0 && yCoords.BinarySearch(planet.y) > 0).ToList();                                  
        }

    }
}
