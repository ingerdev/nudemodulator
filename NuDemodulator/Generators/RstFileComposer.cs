﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Ninject.Planning.Targets;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{    
    class RstFileComposer
    {
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private struct RstSectionOffsets
        {
            public int ShipX;
            public int TargetX;
            public int Planets;
            public int Bases;
            public int Messages;
            public int ShipXYX;
            public int GenX;
            public int Vcrs;
            public byte[] ToBytes()
            {
                Byte[] bytes = new Byte[Marshal.SizeOf(typeof(RstSectionOffsets))];
                GCHandle pinStructure = GCHandle.Alloc(this, GCHandleType.Pinned);
                try
                {
                    Marshal.Copy(pinStructure.AddrOfPinnedObject(), bytes, 0, bytes.Length);
                    return bytes;
                }
                finally
                {
                    pinStructure.Free();
                }
            }  

        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private struct RstHeader
        {
            public RstSectionOffsets Offsets;            
            public byte[] Signature;       
            public byte[] Subversion;
            public int WinplanDataOffset;
            public int LeechDataOffset;
            public int ExtendedUfoDataOffset;

            public RstHeader(RstSectionOffsets sectionsOffsets,int winplanOffset, int extendedUfoOffset)
            {
                //+0   8 DWORDs  Addresses of the file sections, each + 1
                //now they contains only zeroes, they should be filled furthermore
                //writer.Write(sectionsOffsets.ToBytes());
                //--- Host 3.20+ ---
                //+32   6 BYTEs   Signature "VER3.5" for Winplan RSTs.
                //writer.Write(Utils.GetASCIIString("VER3.5"));
                //+38   2 BYTEs   Sub-Version, currently either "00" or "01".
                //writer.Write(Utils.GetASCIIString("01"));
                //+40     DWORD   File position of the Winplan data, when existent.
                //writer.Write((int)0);
                //+44     DWORD   Position of LEECHx.DAT, if existing (0 otherwise).
                //writer.Write((int)0);
                //--- RST version "01" ---
                //+48     DWORD   Position of extended Ufo database
                //writer.Write((int)0);
                Offsets = sectionsOffsets;
                Signature = Utils.GetASCIIString("VER3.5");
                Subversion = Utils.GetASCIIString("01");
                WinplanDataOffset = winplanOffset;
                LeechDataOffset = 0;
                ExtendedUfoDataOffset = extendedUfoOffset;

            }
            public byte[] ToBytes()
            {
                int headerSize = Marshal.SizeOf(typeof(RstSectionOffsets)) + 6 + 2 + 4 + 4 + 4;
                Byte[] bytes = new Byte[headerSize];
                
                int position = 0;
                int size = Marshal.SizeOf(typeof(RstSectionOffsets));
                byte[] test = Offsets.ToBytes();
                Buffer.BlockCopy(Offsets.ToBytes(), 0, bytes, position, size);
                
                position += size;
                size = 6;
                Buffer.BlockCopy(Signature, 0, bytes, position, size);

                position += size;
                size = 2;
                Buffer.BlockCopy(Subversion, 0, bytes, position, size);

                position += size;                
                byte[] b = BitConverter.GetBytes(WinplanDataOffset);
                b.CopyTo(bytes,position);

                position += b.Length;
                b = BitConverter.GetBytes(LeechDataOffset);
                b.CopyTo(bytes, position);

                position += b.Length;
                b = BitConverter.GetBytes(ExtendedUfoDataOffset);
                b.CopyTo(bytes, position);

                return bytes;

            }  
        }
        public RstFileComposer(Rst rst,string destFileName)
        {
            using (var fileStream = new FileStream(destFileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fileStream))
                {                   
                    writer.Write(createMemoryRst(rst));
                }
            }

        }

        private byte[] createMemoryRst(Rst rst)
        {
            var sectionsOffsets = new RstSectionOffsets();
            RstHeader header = new RstHeader(sectionsOffsets,0,0);
            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memoryStream, Encoding.ASCII, true))
                {
                    writer.Write(header.ToBytes());

  //                  Normally, the data follows at 96 (60h) or 33 (21h), with some
  //uninitialized data between the header and the data.
                    byte[] gapBetweenHeaderAndData = new byte[96 - memoryStream.Length];
                    writer.Write(gapBetweenHeaderAndData);

 //                   --- Section 1 (pointer at +0): Ships ---
                    sectionsOffsets.ShipX = (int)memoryStream.Position;
 //+0     WORD    Number of ships                  
 //+2   n BYTEs   Ship records of 107 bytes each, see SHIPx.DAT                  
                    ShipX.GenerateShipXSection(rst.ships, rst.player.raceid,memoryStream);

 //                   --- Section 2 (pointer at +4): Visual Contacts ---
                    sectionsOffsets.TargetX = (int)memoryStream.Position;
 //+0     WORD    Number of contacts. Must be <= 50 for Wisseman clients.
 //               VPHost and PHost with AllowBigTargets allow more than 50
 //               contacts if the client-side unpacker can handle those
 //               (VPUnpack, k-unpack, CCUnpack).
 //+2   n BYTEs   Records of 34 bytes each, see TARGETx.DAT.
 //               DOS-style RSTs: the 50 nearest contacts.
 //               Windows-style RSTs: the first 50 contacts (in Id order)
                    TargetX.GenerateTargetXSection(rst.ships, rst.player.raceid, memoryStream);

 //                   --- Section 3 (pointer at +8): Planets ---
                    sectionsOffsets.Planets = (int)memoryStream.Position;
 //+0     WORD    Number of planets
 //+2   n BYTEs   Planet records of 85 bytes each, see PDATAx.DAT
                    PdataX.GeneratePdataXSection(rst.planets,rst.ships,rst.player.raceid,memoryStream);
 
//                   --- Section 4 (pointer at +12): Bases ---
                    sectionsOffsets.Bases = (int)memoryStream.Position;
 //+0     WORD    Number of starbases
 //+2   n BYTEs   Starbase records of 156 bytes each, see BDATAx.DAT
                    BDataX.GenerateBDataXSection(rst.starbases,rst.stock,rst.player.raceid,memoryStream);

 //                   --- Section 5 (pointer at +16): Messages ---
                    sectionsOffsets.Messages = (int)memoryStream.Position;
                  
 //+0     WORD    Number of messages
 //+2   n BYTEs   Records of 6 bytes each
 //                +0     DWORD   Address of message in RST file + 1
 //                +4     WORD    Length of message in bytes
 //               The messages usually follow the message headers, and are
 //               encrypted like in MDATAx.DAT.
 //               Reportedly, Winplan cannot unpack results where the number
 //               of messages exceeds 5461, i.e., where the message directory
 //               is larger than 32k.
                    MdataX.GenerateMDataXSection(rst.messages,rst.player.raceid,memoryStream);

//--- Section 6 (pointer at +20): Ship Coordinates ---
                    sectionsOffsets.ShipXYX = (int)memoryStream.Position;
// +0   n BYTEs   500 (or 999) Records of 8 bytes each, see SHIPXYx.DAT:
                    ShipXYX.GenerateShipXYXSection(rst.ships,rst.player.raceid,memoryStream);

                    //--- Section 7 (pointer at +24): GENx.DAT ---
                    sectionsOffsets.GenX = (int)memoryStream.Position;
                    GenX.GenerateGenXSection(rst.scores,rst.game,rst.player.raceid,memoryStream);

 //                   --- Section 8 (pointer at +28): VCRs ---
                    sectionsOffsets.Vcrs = (int)memoryStream.Position;
 //+0     WORD    Number of VCRs
 //+2   n BYTEs   VCR records of 100 bytes each, see VCRx.DAT.
                    VcrX.GenerateVcrXSection(rst.vcrs,rst.player.raceid,memoryStream);

//Winplan data
//-------------

//  The pointer to this section is at offset +40.

// +0 500 RECORDs of 8 bytes each; Mine fields
                    header.WinplanDataOffset = (int)memoryStream.Position;
                    MinefieldSection.GenerateMinefieldSection(rst.minefields,rst.player.raceid,memoryStream);

//+? 600 BYTEs   Ion storms, 50 records of 12 bytes each
                    StormSection.GenerateStormSection(rst.ionstorms,rst.player.raceid,memoryStream);

 //+?  50 RECORDs of 4 bytes each: Explosions
                    ExplosionSection.GenerateExplosionSection(rst.messages,rst.player.raceid,memoryStream);

//+? 682 BYTEs   Contents of RACE.NM. RACE.NM remains unchanged if this
                    RaceNM.GenerateRaceNMSection(rst.races,memoryStream);

 //+? 7800 BYTEs  Contents of UFO.HST, filtered
                    Ufo.GenerateUfoSection(rst.player.raceid,memoryStream);

//+?   4 BYTEs   Signature "1211", if all visual contacts fit into the
//                normal TARGETx.DAT file, "1120" otherwise. These numbers
//                appear to be literals, not some strange flags.
                    if (!TargetX.IsAdditionalContactsPresent(rst.ships, rst.player.raceid))
                    {
                        writer.Write(Utils.GetASCIIString("1211"));                        
                    }
                    else
                    {
                        writer.Write(Utils.GetASCIIString("1120"));
                        //--- Additional Visual Contacts ---
                        TargetX.GenerateAdditionalTargetXSection(rst.ships, rst.player.raceid, memoryStream);
                    }                  

                }

                header.Offsets = sectionsOffsets;
                //good, there we have almost good rst. Now we should rewrite stream's header with valid values               
                byte[] streamContent = memoryStream.ToArray();
                byte[] headerContent = header.ToBytes();
                Buffer.BlockCopy(headerContent,0,streamContent,0,headerContent.Length);

                return streamContent;
            }
            
        }
    }
}
