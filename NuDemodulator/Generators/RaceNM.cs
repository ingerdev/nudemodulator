﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    internal class RaceNM
    {
        public static void GenerateRaceNMFile(List<Race> races, string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                GenerateRaceNMSection(races,fileStream);
            }
            //System.IO.File.WriteAllLines(fileName,  from PlanetName planet in planets select planet.Name);
        }

        public static void GenerateRaceNMSection(List<Race> races, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                //yes i know races[0] have almost similar "unknown" race but i dont trust it,
                //who quarantee it wont be lost sometimes?
                Race emptyRace = new Race() {adjective = "unknown", name = "Unknown", shortname = "Unknown"};

                //filter races = skip doublicates and delete possible "unknown" race
                var filteredRaces = FilterRaces(races).ToList();

                //we need sharp 11 races to be written in rst
                //so if their count is less or greater 11 we will add/skip rest.
                for (int k = 0; k < 11; k++)
                {
                    if (filteredRaces.Count > k)
                    {
                        writeRacenameToStream(filteredRaces[k],writer);
                    }
                    else
                    {
                        writeRacenameToStream(emptyRace, writer);
                    }
                }
            }
        }

        private static void writeRacenameToStream(Race race, BinaryWriter writer)
        {           
// ReSharper disable InconsistentNaming
            const int LONG_RACE_NAME_LENGTH = 30;
            const int SHORT_RACE_NAME_LENGTH = 20;
            const int ADJECTIVE_RACE_NAME_LENGTH = 12;
// ReSharper restore InconsistentNaming
            //+0  330 BYTEs  Long form of 11 names (30 chars): "The Robotic Empire"
            writer.Write(Utils.GetNormalizedString(race.name,LONG_RACE_NAME_LENGTH)); 
            //+330 220 BYTEs  Short form of 11 names (20 chars): "The Robots"
            writer.Write(Utils.GetNormalizedString(race.shortname, SHORT_RACE_NAME_LENGTH)); 
            //+550 132 BYTEs  Adjective of 11 names (12 chars): "Robotic"
            writer.Write(Utils.GetNormalizedString(race.adjective, ADJECTIVE_RACE_NAME_LENGTH)); 

        }
        public static IEnumerable<Race> FilterRaces(IEnumerable<Race> races)
        {
            //there groupby works like distinct
            //im not sure planets.nu cannot repeat races in massive games
            //also planets.nu have zero race, "unknown"
            return
                races
                    .Where(race => !race.name.Equals("unknown",StringComparison.InvariantCultureIgnoreCase))
                    .GroupBy(race => race.name)
                    .Select(groupedRaces => groupedRaces.First());                   
                   
        }


    }
}