﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuDemodulator.Datatypes;
using NuDemodulator.Parsers;

namespace NuDemodulator.Generators
{
    internal class MinefieldSection
    {
        public static void GenerateMinefieldSection(List<Minefield> minefields, int raceid, Stream writeStream)
        {
            using (var writer = new BinaryWriter(writeStream, Encoding.ASCII, true))
            {
                var presentMinefields = minefields.ToDictionary(minefield => minefield.id, minefield => minefield);

                for (int i = 0; i < 500; i++)
                {
                    //ship with id == i not present in our list
                    if (!presentMinefields.ContainsKey(i))
                    {
                        //write 8 bytes of pure zeroes
                        writer.Write((ulong) 0);
                        continue;
                    }
                    //    +0 500 RECORDs of 8 bytes each; Mine fields
                    //+0     WORD    X
                    writer.Write((ushort) presentMinefields[i].x);
                    //+2     WORD    Y
                    writer.Write((ushort) presentMinefields[i].y);
                    //+4     WORD    Radius. Zero if the minefield has been
                    //               swept. All other information remains in
                    //               this minefield slot until the slot is
                    //               re-used for a new minefield.
                    writer.Write((ushort) presentMinefields[i].radius);
                    //+6     WORD    Owner.
                    writer.Write((ushort) presentMinefields[i].ownerid);
                    //                0      empty record
                    //                1..11  Normal minefield belonging to
                    //                       race 1..11
                    //                12     Crystalline Web mine field.
                    //               Note that this does not allow transmission
                    //               of web minefields that do not belong to the
                    //               Crystals. PHost sends non-Crystalline webs
                    //               as normal mines (1..11).
                }
            }
        }
    }
}
